
#!/usr/bin/bash

meta_file="./ris-yiff_party.meta.js"
output_file="./ris-yiff_party.user.js"
src_dir="./src/"
declare -a files=("Main.js" "FilePage.js" "PatreonCreatorPage.js" "OverviewPage.js")

echo "Writing to file: $output_file"
echo "Writing contents from $meta_file"
cat "$meta_file" > "$output_file"
	echo "" >> "$output_file"
	echo "" >> "$output_file"

echo "(() => {" >> "$output_file"
for file in "${files[@]}"; do
	echo "Writing contents from $src_dir/$file"
	cat "$src_dir/$file" >> "$output_file"
	echo "" >> "$output_file"
	echo "" >> "$output_file"
done
echo "})();" >> "$output_file"