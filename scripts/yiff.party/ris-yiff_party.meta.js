// ==UserScript==
// @name         yiff.party (ris)
// @version      2.2.0
// @description  good night, sweet prince
// @author       (You)
// @namespace    ris
// @match        *://yiff.party/*
// @match        *://data.yiff.party/*
// @grant        GM_openInTab
// @grant        GM_addStyle
// @grant        GM_setValue
// @grant        GM_getValue
// @run-at       document-idle
// @updateURL    https://bitbucket.org/Rhiki/rhikis-userscripts/raw/master/scripts/yiff.party/ris-yiff_party.meta.js
// @downloadURL  https://bitbucket.org/Rhiki/rhikis-userscripts/raw/master/scripts/yiff.party/ris-yiff_party.user.js
// @supportURL   https://bitbucket.org/Rhiki/rhikis-userscripts/
// ==/UserScript==