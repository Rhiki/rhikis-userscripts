# yiff.party Userscript

Improve lurking and downloading from yiff.party.

---
## Requirements
Any browser with userscript add-on support should work, though my testing is currently only done with up to date Firefox + Tampermonkey. Feel free to report any issues with any Browser/Userscript combination and I'll look into it.

---
## Features
### Patreon Creator page
- **Hide "support artist" banner**
- **Store information (locally) about last visit and already seen posts per creator page.**
    - Last visit date, displayed as hint.
    - IDs for all seen posts. Used to highlight already seen posts on future visits and display information about not yet seen posts (on current page, and total).
    - Amount of shared files during visit. Used to highlight the "Shared files" tab when new files were added since the last vist.
- **Buttos to automatically open all links on the current page or per post.**
    - Links are filtered down a little:
        - Certain domains are excluded from automated opening:
            - for example: patreon.com, gumroad.com, etc.
        - Certain file names are excluded from being opened automatically:
            - Files that are just preview thumbnails of external hosters (YouTube, Google Drive, Dropbox, Vimeo, etc.)
    - Links that share the same file name with other links in the current post will be skipped past the first one. For example when a post contains two files called `Image-1.png` it will only be opened once. This may cause unintended behaviour if the creator uploads files with the same name intentionally, but it stops "post file" links to open multiple files of the same image.
- **Highlight favorite creators on the overview page**
    
    For this to work you need to have the creator's page visited at least once (after version 2.1.0), so that the userscript can map the creator names to their ID on yiff.party. Then the entries in the favorites list will be colorcoded as follows:
    - Green: No new posts or shared files
    - Red: New posts or shared files
    - Purple: Error while checking/Unable to check
- **Highlight already clicked links.**
- **Adds a text area where all links on the current page are dispalyed, for easy copying into a download manager.**
- Appends an "override" file name that is used with my [Save/Copy Hotkey]() userscript. It doesn't affect any browser behaviour if the userscript isn't installed. Shared files also supported. *TODO: Add link to save/copy userscript. Config to disable this feature.*

### File pages
- **Automatically redirect when file name gives away an external host.**
    - Currently only implemented for gfycat/redgifs gifs.

---
## Building
Remember to increase the version in `./ris-yiff_party.meta.js` accordingly ([Semantic Versioning](https://semver.org/)).
 - Linux <br />
    The build script is a bit messy with adding `(() => {/*code*/})();` and new lines, but it does the job and allows files to be cleaner.
    <br />
   1. Execute `./build.sh`
   2. Built file is located in `./ris-yiff_party.user.js`