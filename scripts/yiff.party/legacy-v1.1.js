// ==UserScript==
// @name         yiff.party (ris) - legacy 1.0 version
// @namespace    ris
// @version      1.1
// @description  Improve yiff.party browsing
// @author       (You)
// @match        *://yiff.party/*
// @match        *://data.yiff.party/*
// @require      https://openuserjs.org/src/libs/sizzle/GM_config.js
// @grant        GM_openInTab
// ==/UserScript==

/**
 * IMPORTANT NOTICE: This is an old and ugly version (1.1) of the yiff.party userscript, it's recommended
 * to not use it, but feel free to do whatever you want. It's mainly included because some stuff
 * still needs to be put into the new version (2.x), mainly configurations.
 */

(function () {
    'use strict';
    if (document.location.href.includes("data.yiff.party")) {
        filePage();
    } else {
        creatorPage();
    }
})();



function filePage() {

    const backNavigation = window.performance.navigation.type === 2;

    if (!backNavigation && document.location.href.includes("https_thumbs.gfycat.com")) {
        gfycatRedirect();
        console.log("hello");
    }

    function gfycatRedirect() {
        const url = document.location.href;
        const start = url.lastIndexOf("/") + 1;
        const end = url.indexOf("#") > 0 ? url.indexOf("#") : url.length - 1;
        const sub = url.substring(start, end);
        const ext = sub.substr(sub.lastIndexOf("."))
        const overrideFilename = (url.indexOf("#") > 0 ? url.substring(url.indexOf("#")) : "").replace(sub, "").trim() + ext;
        const gfycatUrl = sub.replace("https_", "https://").replace(".com_", ".com/");
        const gotoQuery = "?ris-goto=0";
        document.location.href = gfycatUrl + gotoQuery + overrideFilename;
    }

}

function creatorPage() {
    var hideSupportBanner;
    let creatorId = document.location.href.substr(document.location.href.lastIndexOf("/") + 1);
    const LOCAL_STORAGE_KEY = "YIFF_PARTY_RHIKI_USER_JS";
    var storage = {};
    const artistName = getArtistName();
    const bannedDomains = [
        // Patreon domains
        "www.patreon.com", "patreon.com",
        // Gumroad domains
        "gum.co","gumroad.com",
        // Others
        "picarto.tv", "www.etsy.com", "etsy.com", "www.deviantart.com", "discord.gg"];
    const regexGdriveThumbnails = /.*https_lh\d+\.googleusercontent\.com_.*/i;
    const regexDropboxThumbnails = /.*https?_www\.dropbox\.com_static.*/i;
    const regexPornhubThumbnails = /.*https_di\.phncdn.com_.*/i;

    init();

    function removeSupportBanner() {
        const sheet = document.styleSheets[0];
        sheet.insertRule(".support-start{visibility: hidden;}");
    }

    function highlighVisitedLinksCss(){
        const sheet = document.styleSheets[0];
        sheet.insertRule("a:visited{color: red;}");
    }

    function showAlreadySeenPosts(show){
        const alreadySeenClass = ".ris-yiff-already-seen";
        const sheet = document.styleSheets[0];
        const value = show ? "inherit" : "none";
        const cssRule = sheet.cssRules.item(alreadySeenClass);
        if (cssRule.selectorText === alreadySeenClass) {
            cssRule.style.display = value;
        } else {
            sheet.insertRule(`.ris-yiff-already-seen{ display: ${value};}`);
        }
    }

    function init() {
        initConfig();
        applySettings();
        executeSettings();
        addSettingsButton();
        //favesUpdated();
        highlighVisitedLinksCss();
        handleLastVisit();
        linkTextAreaAndButtons();
        initPageDependent();
        handlePagination();
        addOpenAllLinksButton();
        addShowOnlyUnseenButton();
    }

    function initPageDependent(){
        handleSeenPosts();
        improvedLinks();
        addAllLinks();
        addOpenAllPostLinksButtons();
    }

    function shouldOpen(a){
        if (bannedDomains.includes(a.hostname)) return false;
        else if (regexGdriveThumbnails.test(a.href)) return false;
        else if (regexDropboxThumbnails.test(a.href)) return false;
        else if (regexPornhubThumbnails.test(a.hreF)) return false;
        else return true;
    }

    function areNamesEqual(first, second){
        return first.toLowerCase().replace(/[\W|_]/g, "") == second.toLowerCase().replace(/[\W|_]/g, "");
    }

    function getArtistName(){
        const fullName = document.querySelector(".yp-info-name").innerText;
        const firstPart = fullName.substring(0, fullName.lastIndexOf("(")).trim();
        const secondPart = fullName.substring( fullName.lastIndexOf("(") + 1, fullName.lastIndexOf(")")).trim();
        if (areNamesEqual(firstPart, secondPart)) return firstPart;
        return `${firstPart} (${secondPart})`;
    }

    function addAllLinks(){
        const links = getAllLinksOnPage();
        document.querySelector("#yp-userjs-linkarea").value = links.join("\n");
        document.querySelector("#yp-userjs-linkarea-status").innerText = `Found ${links.length} links on this page!`;
    }

    function findAllUniqueLinksInCard(cardAction, excludeElement){
        const alreadyOpened = [];
        const result = []
        Array.from(cardAction.parentElement.querySelectorAll("a")).slice().reverse().forEach(link => { // Update 21.06.2020: Reverse order with [...x].slice().reverse()
            if (link != excludeElement && shouldOpen(link)){
                const end = link.href.includes("#") ? link.href.indexOf("#") : link.href.length - 1;
                const filename = link.href.substring(link.href.lastIndexOf("/") + 1, end).replaceAll(" ","_");
                if(filename == "" || !alreadyOpened.includes(filename)){
                    result.push(link.href);
                    alreadyOpened.push(filename);
                }
            }
        });
        return result;
    }

    function getAllLinksOnPage(){
        let links = [];
        document.querySelectorAll(".card-action").forEach(cardAction => {links = links.concat(findAllUniqueLinksInCard(cardAction)) });
        return links;
    }

    function linkTextAreaAndButtons(){
        let linkContainer = document.createElement("div");
        let info = document.createElement("span");
        info.id = "yp-userjs-linkarea-status";
        info.style.marginLeft = "10vw";
        linkContainer.appendChild(info);
        linkContainer.appendChild(document.createElement("br"));
        let input = document.createElement("textarea")
        input.style.marginLeft = "10vw";
        input.style.width = "80vw";
        input.id = "yp-userjs-linkarea";
        input.rows = "4";
        linkContainer.appendChild(input);
        document.querySelector("#index-banner").appendChild(linkContainer);
    }

    function addOpenAllPostLinksButtons(){
        document.querySelectorAll("#posts .card-action").forEach(action => {
            const a = document.createElement("a");
            a.innerText = "🗁";
            a.classList.add("yiff-ris-open-all-links");
            a.style.cursor = "pointer";
            a.addEventListener("click", _ => {
                // Update 11.06.2020: Sort array
                // see: https://stackoverflow.com/questions/2802341/javascript-natural-sort-of-alphanumerical-strings
                const collator = new Intl.Collator(undefined, {numeric: true, sensitivity: 'base'});
                //const sorted = myArray.sort((a, b) => collator.compare(a.name, b.name))
                const unsorted = findAllUniqueLinksInCard(action, a);
                const sorted = unsorted.sort(collator.compare);
                unsorted.reverse().forEach(link => GM_openInTab(link, true)); // TODO/FIXME: Went back to unsorted because this collator seems really unreliable.
            });
            action.prepend(a);
        });
    }

    let showSeenPosts = true;
    function addShowOnlyUnseenButton() {
        let buttonDiv = document.createElement("div")
        buttonDiv.classList.add("yp-info-tags");
        buttonDiv.style.marginTop = "2px";
        let buttonA = document.createElement("a");
        buttonA.classList.add("waves-effect", "waves-light", "btn");
        buttonA.innerText = "Show only unseen files";
        buttonA.addEventListener("click", () => {
            showSeenPosts = !showSeenPosts;
            showAlreadySeenPosts(showSeenPosts);
        });
        buttonDiv.appendChild(buttonA);
        document.querySelector("div.yp-info-co3").appendChild(buttonDiv);
    }

    function addOpenAllLinksButton(){
        let buttonDiv = document.createElement("div")
        buttonDiv.classList.add("yp-info-tags");
        buttonDiv.style.marginTop = "2px";
        let buttonA = document.createElement("a");
        buttonA.classList.add("waves-effect", "waves-light", "btn");
        buttonA.innerText = "Open all files";
        buttonA.addEventListener("click", () => {
            // Hacky
            document.querySelectorAll(".yiff-ris-open-all-links").forEach(a => a.click());
        });
        buttonDiv.appendChild(buttonA);
        document.querySelector("div.yp-info-co3").appendChild(buttonDiv);
    }

    function improvedLinks(){
        document.querySelectorAll(".card-action a").forEach(a => {
            // links to external pages not possible here so no need to check
            const title = a.parentElement.parentElement.querySelector("span.card-title");
            const date = a.parentElement.parentElement.querySelector("span.post-time");
            if (title && date) a.href = buildFileName(a, title.innerText.replace("\nmore_vert", ""), date.innerText);
        });
        document.querySelectorAll(".card-attachments a").forEach(a => {
            if(a.href.includes("data.yiff.party")){
                //kinda hacky but whatever
                const cardTitle = a.parentElement.parentElement.parentElement.querySelector("span.card-title").innerText;
                const matches = cardTitle.match(/(.+) (\d{4}-\d{2}-\d{2} \d{2}:\d{2}).*/);
                if (matches == null) return; //TODO: Figure out why
                const title = matches[1];
                const date = matches[2];
                a.href = buildFileName(a, title, date);
            }
        });
        document.querySelectorAll(".post-img-inline").forEach(img => {
            if (img.attributes["data-src"].value.includes("patreon_inline")){
                //TODO: Remove code duplication from above
                //kinda hacky but whatever
                const cardTitle = img.parentElement.parentElement.parentElement.parentElement.querySelector("span.card-title");
                if (cardTitle == null) return; // TODO: Figure out why
                const matches = cardTitle.innerText.match(/(.+) (\d{4}-\d{2}-\d{2} \d{2}:\d{2}).*/);
                if (matches == null) return; //TODO: Figure out why
                const title = matches[1];
                const date = matches[2];
                const indexInPost = [...img.parentElement.parentElement.parentElement.childNodes].indexOf(img.parentElement.parentElement);
                img.parentElement.href = buildFileName(img.parentElement, title + " " + indexInPost, date);
            }
        });

        function buildFileName(a, title, date){
            let filename = a.href.substring(a.href.lastIndexOf("/") + 1, a.href.lastIndexOf("."))
            let extension = a.href.substr(a.href.lastIndexOf("."));
            date = date.replace(/[ :-]+/g, "");
            title = title.replace(/[/]+/g, "_");
            return `${a.href}#[${artistName}] ${title} (${date}) ${filename}${extension}`
        }
    }

    function appendToArtistInfo(id, text){
        let p = document.querySelector(`#${id}`);
        if (p !== null){
            p.innerText = text;
        } else {
            p = document.createElement("p");
            p.id = id;
            p.innerText = text;
            p.style.lineHeight = "1";
            p.style.margine = "0";
            document.querySelector(".yp-info-check").prepend(p);
        }
    }

    function handleLastVisit(){
        let previousVisits = readStorage("previous-visits", {});
        let previousVisit = "never";
        if (previousVisits[creatorId] != undefined){
            previousVisit = previousVisits[creatorId];
        }
        appendToArtistInfo("yp-userjs-lastvisit", `(Last visited: ${previousVisit})`);
        previousVisits[creatorId] = new Date().toISOString().split("T")[0];
        writeStorage("previous-visits", previousVisits);
    }

    function handleSeenPosts(){
        let unseenOnPage = 0;
        let previousSeens = readStorage("previous-seen", {});
        let previousSeen = [];
        if (previousSeens[creatorId] != undefined){
            previousSeen = previousSeens[creatorId];
        }
        const previousSeenCount = previousSeen.length;
        document.querySelectorAll(".yp-post").forEach(post => {
            if (previousSeen.includes(post.id)){
                post.classList.add("ris-yiff-already-seen");
                post.style.border = "2px solid red";
            } else {
                previousSeen.push(post.id);
                unseenOnPage++;
            }
        });
        const unseenTotal = document.querySelector("li.tab:first-child > a").innerText.match(/.*\((\d+)\)/)[1] - previousSeenCount;
        appendToArtistInfo("yp-userjs-seencount", `(Unseen posts on current page: ${unseenOnPage})\n(Unseen posts total: ${unseenTotal})`);
        previousSeens[creatorId] = previousSeen;
        writeStorage("previous-seen", previousSeens);
    }

    function handlePagination(){
        new MutationObserver((mutationsList, observer) => {
            if(!mutationsList[0].target.classList.contains("row-disabled")){
                //row-disabled gets added when pagination happens, removed when its done
                initPageDependent();
            }
        }).observe(
            document.querySelector("#posts"),
            { attributes: true }
        );
    }

    function addHidePostsInput(){
        //Find all card title elements and if their name inclues "Streaming" hide the card
        //document.querySelectorAll(".card-title").forEach(function(e){if(e.innerText.includes("Streaming")){e.parentNode.parentNode.parentNode.style.display="none";}});
    }

    function favesUpdated(){
        var rows = document.querySelectorAll("#creator-table-faves tr.yp-row");
        var currentValues = {};
        rows.forEach(function(e){
            var children = e.querySelector("td");
            var name = children[0];
            var lastPost = children[1];
            var postCount = children[2];
            var fileCount = children[3];
            currentValues[name] = [postCount, fileCount];
        });
        Object.keys(currentValues).forEach(function(key) {
            //console.log(key, dictionary[key]);
        });
    }

    function addSettingsButton(){
        var navBar = document.querySelector("ul.right");
        var mobileNav = document.querySelector("#mobile-nav")
        var li = document.createElement("li");
        var a = document.createElement("a");
        var i = document.createElement("i");
        i.innerText = "settings";
        i.classList.add("material-icons");
        i.classList.add("left");
        a.text = "Userscript Settings";
        a.onclick = openUserScriptSettings;
        a.appendChild(i);
        li.appendChild(a);
        mobileNav.appendChild(li);
        navBar.appendChild(li);
    }

    function openUserScriptSettings(){
        document.querySelector("#yp-mobilenav"); //ToDo Hide
        GM_config.open();
    }

    function applySettings(){
        hideSupportBanner = GM_config.get('HideSupportBanner');
    }

    function executeSettings(){
        if (hideSupportBanner) removeSupportBanner();
    }

    function initConfig() {
        GM_config.init(
            {
                'id': 'at.rhiki.userscripts.yiffparty-ris',
                'title': "Rhiki\'s yiff.party Script configuration",
                'fields':
                {
                    'HideSupportBanner': {
                        'label': 'Hide the pledge banner on the creator page',
                        'type': 'checkbox',
                        'default': true
                    },
                    'OpenPostOnPostFile': {
                        'label': 'Open the post when clicking on "POST FILE".',
                        'type': 'checkbox',
                        'default': true
                    }
                },
                'css': 'body { background-color: #7986cb; color: white}',
                'events':
                {
                    'save': function() { applySettings(); executeSettings(); }
                }
            }
        );
    }

    /**
     * Read a value from the localStorage if possible
     * @param {string} key - the key to retreive
     * @param {*} defaultValue the  default value to return if no value for the key was found
     * @returns the value associated with the key; or the value passed in defaultValue if the key was not found
     */
    function readStorage(key, defaultValue) {
        if (!storage.hasOwnProperty("init")) {
            if (typeof (Storage) !== "undefined") {
                storage = JSON.parse(localStorage.getItem(LOCAL_STORAGE_KEY));
                if (storage === null) {
                    storage = {}; // first time
                }
            }
            storage["init"] = true;
        }
        return storage.hasOwnProperty(key) ? storage[key] : defaultValue;
    }

    /**
* Write a key/value pair to the storage variable and saves it to localStorage if possible
     * @param {string} key - the key to write to
     * @param {any} value - the value to write
     */
    function writeStorage(key, value) {
        storage[key] = value;
        if (typeof (Storage) !== "undefined") {
            localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(storage));
        }
    }
}
