# Version 2.1.2
- ## Changes
    - Fixed sites with primeleap.
    - Fix creator ID parsing when page URL contains query parameters.
    - Added excluded domains:
        - `www.facebook.com`
        - `www.postybirb.com`
        - `www.twitch.tv`
    - Aded link exclude regex for:
        - mega thumbnails
        - *.untitled files
# Version 2.1.2
- ## Changes
    - Added link exclude regex for:
        -  vimeo thumbnails
        - yiffparty vimeo proxy

# Version 2.1.0
- ## Changes
    - ### Favorites highlighting
        - Highlight creators in the favorite list depending on:
            - Green: No new posts or shared files
            - Red: New posts or shared files
            - Purple: Error while checking/Unable to check
    - ### New auomatic link opening exclusion RegExps:
        - Filter out picarto thumbnails and ".jpe" files.
    - ### Apply override filenames on shared files too.
    - ### Redirect logic for redgifs thumbnails.
- ## Known Issues
    - Sites with primeleap aren't working.

# Version 2.0.0
- ## Changes
    -  ### Refactoring
        - Split source files into multiple files for easier navigation, split by which page they are run on.
        - Add regions for code folding
        - Add build.sh file for buidling complete .user.js file
        - Use GM_getValue/GM_setValue instead of localStorage
- ## Known Issues
    - Sites with primeleap aren't working.