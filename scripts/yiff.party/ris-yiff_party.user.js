// ==UserScript==
// @name         yiff.party (ris)
// @version      2.2.0
// @description  good night, sweet prince
// @author       (You)
// @namespace    ris
// @match        *://yiff.party/*
// @match        *://data.yiff.party/*
// @grant        GM_openInTab
// @grant        GM_addStyle
// @grant        GM_setValue
// @grant        GM_getValue
// @run-at       document-idle
// @updateURL    https://bitbucket.org/Rhiki/rhikis-userscripts/raw/master/scripts/yiff.party/ris-yiff_party.meta.js
// @downloadURL  https://bitbucket.org/Rhiki/rhikis-userscripts/raw/master/scripts/yiff.party/ris-yiff_party.user.js
// @supportURL   https://bitbucket.org/Rhiki/rhikis-userscripts/
// ==/UserScript==

(() => {
    'use strict';
    /**
     * Enables the migration of stored values from any 1.x version to 2.x version.
     * Should only be enabled once to avoid overwriting new data.
     */
    const enableV1toV2Migration = false;

    migrations();


    const filePageRegex = /https?:\/\/data\.yiff\.party\/.*/i;
    const patreonCreatorPageRegex = /https?:\/\/yiff\.party\/patreon\/(?<patreon_creator_id>\d+)/i;
    const overviewPageRegex = /https?:\/\/yiff\.party\/?$/i;
    const url = document.location.href;
    if (filePageRegex.test(url)) {
        filePage();
    } else if (patreonCreatorPageRegex.test(url)) {
        patreonCreatorPage();
    } else if (overviewPageRegex.test(url)) {
        overviewPage();
    }


    //#region migrate storage between major versions

    /**
     * Handle migration updates.
     */
    function migrations() {
        if (enableV1toV2Migration) migrateFromV1toV2();
    }


    function migrateFromV1toV2() {
        const storedV1 = JSON.parse(localStorage.getItem("YIFF_PARTY_RHIKI_USER_JS"));
        const result = {}
        const previousSeen = storedV1["previous-seen"];
        for (const key in previousSeen) {
            if (result[key] === undefined) {
                result[key] = {};
            }
            result[key].previouslySeen = previousSeen[key];
        }
        const previousVisits = storedV1["previous-visits"];
        for (const key in previousVisits) {
            if (result[key] === undefined) {
                result[key] = {};
            }
            result[key].lastVisit = previousVisits[key];
        }
        for (const key in result) {
            if (key.includes("#")) { // keys accidentally added in experimental versions
                delete result[key];
                continue;
            }
            GM_setValue(key, result[key]);
        }
        console.log(`yiff.party (ris): Migrated ${Object.keys(result).length} creator data sets from v1 to v2`);
    }

    //#endregion

    function filePage() {
        class Redirector {
            /**
             *  
             * @param {String} name - human readable name for identifying the Redirector
             * @param {RegExp} regex - Regex to use for testing/redirecting URLs; may be undefined
             * @param {Redirector~redirectLogic} redirectLogic - the redirect logic
             */
            constructor(name, regex, redirectLogic) {
                this.name = name;
                this.regex = regex;
                this.redirectLogic = redirectLogic;
            }

            /**
             * Test if the passed URL is valid for use with this Redirector.
             * @param {String} url URL to test
             * @returns true if the URL is valid; false otherwise
             * @todo better way for testing without regex, when needed
             */
            test(url) { return this.regex !== undefined ? this.regex.test(url) : false; }

            /**
             * Applies the redirectLogic function on the passed URL and returns the resulting URL
             * @param {String} url the URL to apply the redirect logic on
             * @returns the URL to which to redirect; undefined if no redirect is possible
             */
            redirect(url) { return this.redirectLogic(url, this.regex); }

            /**
             * Function to apply redirect logic
             * @callback Redirector~redirectLogic
             * @param {string} url the URL to apply the redirect logic on
             * @param {regex} regex the regex that is in use for this Redirector
             * @returns the resulting URL
             */
        }

        const usedBackNavigation = window.performance.navigation.type === 2;
        if (usedBackNavigation) return;

        const possbileFileRedirectors = [
            /**
             * @todo redirectLogic: Move gfycat ris parameter into config?
             */
            new Redirector("gfycat", /^(.*\/)(?<url>https_thumbs\.gfycat\.com_.*?)(#(?<override_filename>.*))?$/i, gfyCatRedgifRedirectLogic),
            new Redirector("redgifs", /^(.*\/)(?<url>https_thcf\d\.redgifs\.com_.*?)(#(?<override_filename>.*))?$/i, gfyCatRedgifRedirectLogic)
        ];

        possbileFileRedirectors.forEach(redirector => {
            if (redirector.test(document.location.href)) {
                const targetUrl = redirector.redirect(document.location.href);
                if (targetUrl !== undefined) {
                    document.location.href = targetUrl;
                }
            }
        });

        /**
         * Redirect logic for both gfycat and redgifs.
         * @param {string} url the URL to apply the redirect logic on
         * @param {RegExp} regex regex to use for extracting the gfycat/redgifs URL.
         *   Requires 2 named groups:
         *     - 'url'
         *     - 'override_filename'
         * @returns the resulting URL
         */
        function gfyCatRedgifRedirectLogic(url, regex) {
            const match = url.match(regex);
            if (match && match.groups) {
                const targetUrl = match.groups["url"].replace(/https?_/, "https://").replace(".com_", ".com/");
                const override = match.groups["override_filename"];
                const gfycatRisParameter = "?ris-goto=0"
                return targetUrl + gfycatRisParameter + "#" + override;
            }
            return undefined;
        }

    }

    function patreonCreatorPage() {

        /**
         * Color to be used in CSS rules for already seen posts and links.
         */
        const seenCssColor = "red"; //@todo: customizable
        /**
         * The creator's ID on yiff.party.
         */
        const creatorId = getCreatorId();
        /**
         * The creator's display name.
         */
        const creatorName = getCreatorName().fullName;
        /**
         * Contains information about the creator's page on yiff.party. Seen posts, last visit, etc.
         */
        const creatorInfo = getStoredCreatorInformation();

        /**
         * A list of domains, that should not be automatically opened when clicking an "Open all links" button.
         */
        const excludeDomains = [
            "www.patreon.com", "patreon.com", "gum.co", "gumroad.com",
            "picarto.tv", "www.etsy.com", "etsy.com", "www.devianart.com", "discord.gg",
            "www.postybirb.com", "www.twitch.tv", "www.facebook.com", "i.pinimg.com",
            "d.va"
        ];

        /**
         * A list of RegExp to be used to filter out links from autoamtically opening, mainly for filtering thumbnail icons.
         */
        const excludeRegex = [
            { name: "*.jpe files", regex: /.*\.jpe$/i }, //it's usually misslabeled thumbnail files,
            { name: "*.Untitled files", regex: /.*\.untitled$/i },
            { name: "Google Drive Thumbnails", regex: /.*https_lh\d+\.googleusercontent\.com_.*/i },
            { name: "Dropbox Thumbnails", regex: /.*https?_www\.dropbox\.com_static.*/i },
            { name: "Pornhub Thumbnails", regex: /.*https_di\.phncdn.com_.*/i },
            { name: "Picarto Thumbnails", regex: /.*https_picarto.tv_.*/i },
            { name: "Vimeo Thumbnails", regex: /.*https_i\.vimeocdn\.com_/i },
            { name: "yiff.party vimeo proxy", regex: /^https?:\/\/yiff\.party\/vimeo\/.*$/i },
            { name: "mega.co.nz Thumbnails", regex: /.*https_cms(\d)*\.mega\.nz/i },
            { name: "wikia image links", regex: /https?:\/\/vignette(\d)*\.wikia\.nocookie\.net.*/i }
        ];

        let seenPostsVisible = true;

        init();

        /**
         * Initialize static components, that only need to be executed once per creator page.
         */
        function init() {
            hideSupportBanner();
            highlightVisitedLinks();
            showAndUpdateLastVisit();
            addOpenAllLinksButtonToCreatorBox();
            addToggleSeenPostsToCreatorBox();
            addLinkDumpTextArea();
            highlightNewSharedFiles();
            highlightPreviouslySeenPosts();
            updateUniqueNameToYiffIdEntry();
            applyOverrideFileNamesOnSharedFiles();

            initPageDependent();

            // Add observer to get notified about page changes.
            new MutationObserver((mutationsList, observer) => {
                if (!mutationsList[0].target.classList.contains("row-disabled")) {
                    //row-disabled gets added when pagination happens, and removed when its done
                    initPageDependent();
                }
            }).observe(document.querySelector("#posts"), { attributes: true });
        }

        /**
         * Initialize components, that need to be re-initialized with each page change on the creator page.
         */
        function initPageDependent() {
            highlightAndUpdatePreviouslySeenPosts();
            megafy();
            applyOverrideFileNames(); // TODO: Configurable/Disable
            addOpenAllLinksToPosts();
            addAllLinksToLinkDump();
        }

        //#region meta/information loading

        /**
         * Fetches the creator's yiff.party ID from the URL.
         * If there are any parameters after the ID (for example page to open) it will be removed.
         * Examples:
         * https://yiff.party/patreon/12345678 => 12345678
         * https://yiff.party/patreon/12345678?p=14 => 12345678
         */
        function getCreatorId() {
            const id = document.location.href.substr(document.location.href.lastIndexOf("/") + 1);
            return id.includes("?") ? id.replace(/\?.*/, "") : id;
        }

        /**
         * Fetches the creator's name.
         * @returns {string} the creator's name, "original name" is omitted if the display name is equal to it.
         */
        function getCreatorName() {
            const fullName = document.querySelector(".yp-info-name").innerText;
            const regex = /^(?<first>.*) \((?<second>.*)\)$/i;
            let first, second;
            ({ first, second } = fullName.match(regex).groups);
            return { fullName: areNamesEqual(first, second) ? first : `${first} (${second})`, uniqueName: second };

            /**
             * Compares both names to be equal, case-insensitive. Special characters are stripped before comparision.
             * May add heuristics later on.
             * @param {string} first first part of name (display name)
             * @param {string} second second part of name (original name)
             * @returns true if names are equal; false otherwise
             */
            function areNamesEqual(first, second) {
                const regex = /[\W|_]/g;
                return first.toUpperCase().replace(regex, "") === second.toUpperCase().replace(regex, "");
            }
        }

        /**
         * Load stored information about the creator: Last visit, seen posts, etc.
         */
        function getStoredCreatorInformation() {
            return GM_getValue(creatorId, {});
        }

        /**
         * Update the stored information about the creator.
         */
        function updateCreatorInfo() {
            GM_setValue(creatorId, creatorInfo);
        }

        function updateUniqueNameToYiffIdEntry() {
            const name = getCreatorName().uniqueName;
            const dict = GM_getValue("uniqueNames", {});
            //TODO: Check removed because it seems to cause problems?
            //if (dict[name] !== creatorId) {
            // Very hacky: set a new localStorage value, to later gather them in the overview page.
            // Otherwise we have concurrency problems with multibe tabs overwriting each other's data.
            localStorage.setItem("NEW_CREATOR_ID_" + creatorId, name);
            //}
        }

        //#endregion

        //#region DOM modifications

        /**
         * Appends a line of text to the creator info box. If an id is provided that line of text
         * will be replaced with the supplied text, otherwise a new line will be created.
         * @param {string} id the DOM ID of the text; may be undefined
         * @param {string} text the text to insert
         */
        function appendToArtistInfo(id, text) {
            let p = id !== undefined ? document.querySelector("#" + id) : null;
            if (p !== null) {
                p.innerText = text;
            } else {
                p = document.createElement("p");
                p.id = id;
                p.innerText = text;
                p.style.lineHeight = "1";
                p.style.margine = "0";
                document.querySelector(".yp-info-check").prepend(p);
            }
        }

        /**
         * Add a line to show the last visit on this page to the creator info box.
         * Stores the current date as last visit date afterwards.
         */
        function showAndUpdateLastVisit() {
            let lastVisit = creatorInfo.lastVisit !== undefined ? creatorInfo.lastVisit : "never";
            appendToArtistInfo("ris-lastvisit", `(Last visited: ${lastVisit})`);
            creatorInfo.lastVisit = new Date().toISOString().split("T")[0];
            updateCreatorInfo();
        }

        /**
         * Highlight all previously seen posts on the current page. Only adds a css-class to
         * fitting posts, actually highlighting logic is in {@link highlightPreviouslySeenPosts}.
         * Adds lines describing the amounts of unseen posts on the current page and overall to
         * the creator info box.
         * Afterwards stores all posts on the current page as seen in the creator info.
         * Important: this function has to be called on each page change.
         */
        function highlightAndUpdatePreviouslySeenPosts() {
            let unseenOnPage = 0;
            if (creatorInfo.previouslySeen === undefined) {
                creatorInfo.previouslySeen = [];
            }
            const previouslySeenCount = creatorInfo.previouslySeen.length;
            document.querySelectorAll(".yp-post").forEach(post => {
                if (creatorInfo.previouslySeen.includes(post.id)) {
                    post.classList.add("ris-previously-seen");
                } else {
                    creatorInfo.previouslySeen.push(post.id);
                    unseenOnPage++;
                }
            });
            const unseenTotal = document.querySelector("li.tab:first-child > a").innerText.match(/.*\((\d+)\)/)[1] - previouslySeenCount;
            appendToArtistInfo("ris-seen-count", `(Unseen posts on current page: ${unseenOnPage})\n(Unseen posts total: ${unseenTotal})`);
            updateCreatorInfo();
        }

        /**
         * Adds a button that opens all links on the page to the creator info box.
         */
        function addOpenAllLinksButtonToCreatorBox() {
            addButtonToCreatorBox("Open all files", () => {
                document.querySelectorAll(".ris-open-all-links").forEach(link => link.click())
            });
        }

        /**
         * Adds a button to toggle visibility of already seen posts.
         */
        function addToggleSeenPostsToCreatorBox() {
            addButtonToCreatorBox("Toggle seen posts", () => {
                seenPostsVisible = !seenPostsVisible;
                GM_addStyle(`.ris-previously-seen { display: ${ seenPostsVisible ? "block" : "none"} }`);
            });
        }

        /**
         * Utility function to add a button with a given inner text and action to the creator box.
         */
        function addButtonToCreatorBox(text, onClick) {
            const div = document.createElement("div");
            div.classList.add("yp-info-tags");
            div.style.marginTop = "2px";
            const a = document.createElement("a");
            a.classList.add("waves-effect", "waves-light", "btn");
            a.innerText = text;
            a.addEventListener("click", onClick);
            div.appendChild(a);
            document.querySelector("div.yp-info-co3").appendChild(div);
        }

        /**
         * Checks if an anchor tag should be opened for automatic link clicking.
         * @param {HTMLElement} a the link to check
         * @returns true if the link should be opened; false otherwise
         */
        function shouldOpen(a) {
            if (a.href === document.location.href) return false;
            if (excludeDomains.includes(a.hostname)) return false;
            for (let i = 0; i < excludeRegex.length; i++) {
                if (excludeRegex[i].regex.test(a.href)) return false;
            }
            return true;

        }

        /**
         * Finds all links that link to an unique file in a single post.
         * @param {HTMLElement} post
         * @returns {string[]} an array continaing all unique links in the post.
         */
        function findAllUniqueLinksInCard(post) {
            const result = {};
            [...post.querySelectorAll("a")].reverse().forEach(a => {
                if (shouldOpen(a)) {
                    const end = a.href.includes("#") ? a.href.indexOf("#") : a.href.length - 1;
                    const filename = a.href.substring(a.href.lastIndexOf("/") + 1, end).replaceAll(" ", "_");
                    if (result[filename] === undefined) {
                        result[filename] = a.href;
                    }
                }
            });
            return Object.values(result);
        }

        /**
         * Adds a button to open all links in the post at once.
         */
        function addOpenAllLinksToPosts() {
            document.querySelectorAll("#posts .yp-post").forEach(post => {
                const actions = post.querySelector(".card-action");
                if (actions !== null) {
                    const a = document.createElement("a");
                    a.innerText = "🗁";
                    a.classList.add("ris-open-all-links");
                    a.style.cursor = "pointer";
                    a.href = document.location.href;
                    a.addEventListener("click", evt => {
                        const unsorted = findAllUniqueLinksInCard(post);
                        // FIXME/TODO: Collator seemed rather unreliable, so returned to unsorted for now.
                        //const collator = new Intl.Collator(undefined, {numeric: true, sensitivity: 'base'});
                        //const sorted = unsorted.sort(collator.compare);
                        //sorted.forEach(link => GM_openInTab(link, true));
                        unsorted.reverse().forEach(link => GM_openInTab(link, true));
                        evt.preventDefault();
                    });
                    actions.prepend(a);
                }
            });
        }

        /**
         * Adds a textarea below the banner to put all links on the page into for easy copying into a download manager.
         * For filling the textarea see {@link addAllLinksToLinkDump}.
         */
        function addLinkDumpTextArea() {
            const container = document.createElement("div");
            container.classList.add("row");
            const linkCount = document.createElement("span");
            linkCount.id = "ris-linkarea-count";
            container.append(linkCount);
            container.appendChild(document.createElement("br"));
            const linkDump = document.createElement("textarea");
            linkDump.id = "ris-linkarea";
            linkDump.rows = 5;
            container.appendChild(linkDump);
            document.querySelector("#index-banner > .container").appendChild(container);
        }

        function addAllLinksToLinkDump() {
            let links = [];
            document.querySelectorAll("#posts .yp-post").forEach(post => {
                links = links.concat(findAllUniqueLinksInCard(post));
            });
            document.querySelector("#ris-linkarea").value = links.join("\n");
            document.querySelector("#ris-linkarea-count").innerText = `Found ${links.length} links on this page!`;
        }

        /**
         * Highlights the "Shared Files" tab if new shared files were uploaded since the last visit.
         */
        function highlightNewSharedFiles() {
            const link = document.querySelector(".tab:last-of-type > a");
            const count = parseInt(link.innerText.match(/SHARED FILES \((?<count>\d+)\)/).groups.count);
            if (count > (creatorInfo.sharedFileCount !== undefined ? creatorInfo.sharedFileCount : 0)) {
                link.innerText = "NEW! " + link.innerText;
                link.style.color = "red";
                creatorInfo.sharedFileCount = count;
                updateCreatorInfo();
            }
        }

        //#endregion

        //#region CSS modifications

        /**
         * Hides the "Support artist" banner that pops up.
         */
        function hideSupportBanner() {
            GM_addStyle(".support-start { visibility: hidden}; }");
        }

        /**
         * Changes the color of alraedy visited links to be more easily distinguishable.
         */
        function highlightVisitedLinks() {
            GM_addStyle(`a:visited { color: ${seenCssColor}; }`);
        }

        /**
         * Highlight previously seen posts.
         */
        function highlightPreviouslySeenPosts() {
            GM_addStyle(`.ris-previously-seen { border: 2px solid ${seenCssColor}; }`);
        }

        //#endregion

        //#region link modification

        /**
         * Appends a filename, that is to be used for saving with my "Save hotkey"-Userscript.
         * "Override filenames" are appened to the original filenames, behind an anchor "#".
         * It will have no affect on anything if that userscript isn't used.
         * Override file names include artist name, post title, post date and the original file name.
         */
        function applyOverrideFileNames() {
            /**
             * Regex used to extract filename and file extension from a link.
             */
            const fileNameRegex = /https?:\/\/(data\.)?yiff\.party\/.*\/(?<filename>.*)\.(?<extension>.*)$/i;

            document.querySelectorAll("div.yp-post").forEach(post => {
                let titleElement = post.querySelector("span.card-title");
                let dateElement = post.querySelector("span.post-time")
                const title = titleElement === null ? "-" : titleElement.innerText.replace("\nmore_vert", "");
                const date = dateElement === null ? "-" : dateElement.innerText;
                post.querySelectorAll("a").forEach(a => {
                    a.href = buildOverrideFileName(a.href, title, date);
                });
            });

            /**
             * Builds the override file name for the given link in the format of:
             * $href [$creatorName] $title ($date) $filename.$ext.
             * @param {string} href - the hypertext reference of the link
             * @param {string} title - the title of the post
             * @param {string} date - the post of the date
             * @returns the built override file name.
             */
            function buildOverrideFileName(href, title, date) {
                const match = href.match(fileNameRegex);
                if (match === null) {
                    return href;
                }
                const filename = match.groups.filename;
                const extension = match.groups.extension;
                date = date.replace(/[ :-]+/g, "");
                title = title.replace(/[/]+/g, "_");
                return `${href}#[${creatorName}] ${title} (${date}) ${filename}.${extension}`;
            }
        }


        /**
         * Appends an override filename to files on the shared file tab.
         * For files that can't be displayed (mainly .rar and .zip) the default download
         * action will be intercepted and instead a download with GM_download started
         * to use the override filename.
         * For more info on override filenames check {@link applyOverrideFileNames}.
         */
        function applyOverrideFileNamesOnSharedFiles() {
            document.querySelectorAll("#shared_files .card-action a").forEach(a => {
                const fileName = a.href.substring(a.href.lastIndexOf("/") + 1);
                const extension = a.href.substring(a.href.lastIndexOf(".") + 1);
                const overrideName = `[${creatorName}] (Shared File) - ${fileName}`;
                a.href += "#" + overrideName;
                if (!canBeDisplayed(extension)) {
                    a.addEventListener("click", evt => {
                        evt.preventDefault();
                        GM_download(a.href, overrideName);
                    });
                }
            });
        }

        /**
         * Checks if a given file can be displayed (images, videos, etc.) or must be downloaded (archives, .psd, etc.).
         * @param {string} extension file extension, can include the leading '.' but doesn't have to.
         * @returns true, if the file can be displayed in the browser; false otherwise
         */
        function canBeDisplayed(extension) {
            // PDF is not included as the download userscript can't handle it properly (yet?)
            return /\.?(jpe?g|png|gif|webm|mp4|txt|mp3)/i.test(extension);
        }

        //#endregion

        //#region mega linkify

        /**
         * Fix all mega links that are children of the passed _root_ element.
         * @param {Element} root the element from which to look for mega links. Defaults to _document.body_ if it is undefined
         */
        function megafy(root = document.body) {
            root.querySelectorAll(`a[href*="mega.co.nz"]`).forEach(megaLink => megafyLink(megaLink));
        }

        /**
         * Fixes a single mega link element.
         * @param {Element} anchor the mega link node to fix
         */
        function megafyLink(anchor) {
            const nextSibling = megaLink.nextSibling;
            if (anchor.href.endsWith("#") && nextSibling !== undefined && nextSibling.nodeName === "#text") {
                const hash = extractMegaHash(nextSibling.textContent.trim());
                if (hash !== undefined) {
                    anchor.href += hash;
                    anchor.innerText += hash;
                    nextSibling.textContent = nextSibling.textContent.replace(hash, "");
                }
            }

            function extractMegaHash(text) {
                const match = text.match(/(?<hash>.+?)(\s|$)/i);
                if (match !== null && match.groups !== undefined) {
                    return match.groups.hash;
                }
                return undefined;
            }
        }

        //#endregion

    }

    function overviewPage() {
        const NEW_ID_KEY_PREFIX = "NEW_CREATOR_ID_"
        const NAME_TO_ID_MAP_KEY = "nameToIdMap";
        const nameToIdMap = GM_getValue(NAME_TO_ID_MAP_KEY, {});
        const favoritesTable = document.querySelector("#creator-table-faves");

        parseNewIds();
        checkCurrentFavouritesPageForNewPostsOrFiles();
        createObserverForFavoritePagination();

        /**
         * Parse the localStorage for any new IDs put in from creator pages.
         * If new IDs were found, add them into the nameToIdMap, save it with GM_setValue, and delete it from localStorage.
         */
        function parseNewIds() {
            const newIds = [];
            for (const key in localStorage) {
                if (key.startsWith(NEW_ID_KEY_PREFIX)) {
                    const id = key.replace(NEW_ID_KEY_PREFIX, "");
                    const name = localStorage[key];
                    newIds.push({ id: id, name: name });
                    localStorage.removeItem(key);
                }
            }
            if (newIds.length > 0) {
                newIds.forEach(id => {
                    nameToIdMap[id.name] = id.id;
                });
                GM_setValue(NAME_TO_ID_MAP_KEY, nameToIdMap);
            }
        }

        /**
         * Start an observer to listen for pagination events of the favorites table.
         */
        function createObserverForFavoritePagination() {
            const observer = new MutationObserver((mutationsList, observer) => {
                // Any mutation triggered comes from pagination, so just always execute it.
                checkCurrentFavouritesPageForNewPostsOrFiles();
            });
            const config = { childList: true, subtree: true };
            observer.observe(favoritesTable, config)
        }

        /**
         * Check all creators on the favorite list that are currently displayed
         * and highlight them, if they have new posts or shared files.
         */
        function checkCurrentFavouritesPageForNewPostsOrFiles() {
            favoritesTable.querySelectorAll("tr.yp-row").forEach(tr => {
                tr.style.color = getColorForRow(tr);
            });

            function getColorForRow(tr) {
                const name = tr.querySelector("td:first-child").innerText;
                if (nameToIdMap[name] !== undefined) {
                    const creatorInfo = GM_getValue(nameToIdMap[name]);
                    if (creatorInfo !== undefined) {
                        const seenPosts = creatorInfo.previouslySeen.length;
                        const seenSharedFiles = creatorInfo.sharedFileCount;

                        const currentPosts = tr.querySelector("td:nth-child(3)");
                        const currentSharedFiles = tr.querySelector("td:nth-child(4)");
                        if (parseInt(currentPosts.innerText) > seenPosts || parseInt(currentSharedFiles.innerText) > seenSharedFiles) {
                            // New posts or shared files
                            return "red";
                        }
                        // No new posts or shared files
                        return "green";
                    }
                }
                // Could not map name to ID, or no creatorInfo available.
                return "purple";
            }
        }
    }

})();