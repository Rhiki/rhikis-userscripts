function overviewPage() {
    const NEW_ID_KEY_PREFIX = "NEW_CREATOR_ID_"
    const NAME_TO_ID_MAP_KEY = "nameToIdMap";
    const nameToIdMap = GM_getValue(NAME_TO_ID_MAP_KEY, {});
    const favoritesTable = document.querySelector("#creator-table-faves");

    parseNewIds();
    checkCurrentFavouritesPageForNewPostsOrFiles();
    createObserverForFavoritePagination();

    /**
     * Parse the localStorage for any new IDs put in from creator pages.
     * If new IDs were found, add them into the nameToIdMap, save it with GM_setValue, and delete it from localStorage.
     */
    function parseNewIds() {
        const newIds = [];
        for (const key in localStorage) {
            if (key.startsWith(NEW_ID_KEY_PREFIX)) {
                const id = key.replace(NEW_ID_KEY_PREFIX, "");
                const name = localStorage[key];
                newIds.push({id: id, name: name});
                localStorage.removeItem(key);
            }
        }
        if (newIds.length > 0) {
            newIds.forEach(id => {
                nameToIdMap[id.name] = id.id;
            }) ;
            GM_setValue(NAME_TO_ID_MAP_KEY, nameToIdMap);
        }
    }

    /**
     * Start an observer to listen for pagination events of the favorites table.
     */
    function createObserverForFavoritePagination() {
        const observer = new MutationObserver((mutationsList, observer) => {
            // Any mutation triggered comes from pagination, so just always execute it.
            checkCurrentFavouritesPageForNewPostsOrFiles();
        });
        const config = { childList: true, subtree: true };
        observer.observe(favoritesTable, config)
    }

    /**
     * Check all creators on the favorite list that are currently displayed
     * and highlight them, if they have new posts or shared files.
     */
    function checkCurrentFavouritesPageForNewPostsOrFiles() {
        favoritesTable.querySelectorAll("tr.yp-row").forEach(tr => {
            tr.style.color = getColorForRow(tr);
        });

        function getColorForRow(tr) {
            const name = tr.querySelector("td:first-child").innerText;
            if (nameToIdMap[name] !== undefined) {
                const creatorInfo = GM_getValue(nameToIdMap[name]);
                if (creatorInfo !== undefined) {
                    const seenPosts = creatorInfo.previouslySeen.length;
                    const seenSharedFiles = creatorInfo.sharedFileCount;

                    const currentPosts = tr.querySelector("td:nth-child(3)");
                    const currentSharedFiles = tr.querySelector("td:nth-child(4)");
                    if (parseInt(currentPosts.innerText) > seenPosts || parseInt(currentSharedFiles.innerText) > seenSharedFiles) {
                        // New posts or shared files
                        return "red";
                    }
                    // No new posts or shared files
                    return "green";
                }
            }
            // Could not map name to ID, or no creatorInfo available.
            return "purple";
        }
    }
}