'use strict';
/**
 * Enables the migration of stored values from any 1.x version to 2.x version.
 * Should only be enabled once to avoid overwriting new data.
 */
const enableV1toV2Migration = false;

migrations();


const filePageRegex = /https?:\/\/data\.yiff\.party\/.*/i;
const patreonCreatorPageRegex = /https?:\/\/yiff\.party\/patreon\/(?<patreon_creator_id>\d+)/i;
const overviewPageRegex = /https?:\/\/yiff\.party\/?$/i;
const url = document.location.href;
if (filePageRegex.test(url)) {
    filePage();
} else if (patreonCreatorPageRegex.test(url)) {
    patreonCreatorPage();
} else if (overviewPageRegex.test(url)) {
    overviewPage();
}


//#region migrate storage between major versions

/**
 * Handle migration updates.
 */
function migrations() {
    if (enableV1toV2Migration) migrateFromV1toV2();
}


function migrateFromV1toV2() {
    const storedV1 = JSON.parse(localStorage.getItem("YIFF_PARTY_RHIKI_USER_JS"));
    const result = {}
    const previousSeen = storedV1["previous-seen"];
    for (const key in previousSeen) {
        if (result[key] === undefined) {
            result[key] = {};
        }
        result[key].previouslySeen = previousSeen[key];
    }
    const previousVisits = storedV1["previous-visits"];
    for (const key in previousVisits) {
        if (result[key] === undefined) {
            result[key] = {};
        }
        result[key].lastVisit = previousVisits[key];
    }
    for (const key in result) {
        if (key.includes("#")) { // keys accidentally added in experimental versions
            delete result[key];
            continue;
        }
        GM_setValue(key, result[key]);
    }
    console.log(`yiff.party (ris): Migrated ${Object.keys(result).length} creator data sets from v1 to v2`);
}

//#endregion