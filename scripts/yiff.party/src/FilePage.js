function filePage() {
    class Redirector {
        /**
         *  
         * @param {String} name - human readable name for identifying the Redirector
         * @param {RegExp} regex - Regex to use for testing/redirecting URLs; may be undefined
         * @param {Redirector~redirectLogic} redirectLogic - the redirect logic
         */
        constructor(name, regex, redirectLogic) {
            this.name = name;
            this.regex = regex;
            this.redirectLogic = redirectLogic;
        }

        /**
         * Test if the passed URL is valid for use with this Redirector.
         * @param {String} url URL to test
         * @returns true if the URL is valid; false otherwise
         * @todo better way for testing without regex, when needed
         */
        test(url) { return this.regex !== undefined ? this.regex.test(url) : false; }

        /**
         * Applies the redirectLogic function on the passed URL and returns the resulting URL
         * @param {String} url the URL to apply the redirect logic on
         * @returns the URL to which to redirect; undefined if no redirect is possible
         */
        redirect(url) { return this.redirectLogic(url, this.regex); }

        /**
         * Function to apply redirect logic
         * @callback Redirector~redirectLogic
         * @param {string} url the URL to apply the redirect logic on
         * @param {regex} regex the regex that is in use for this Redirector
         * @returns the resulting URL
         */
    }

    const usedBackNavigation = window.performance.navigation.type === 2;
    if (usedBackNavigation) return;

    const possbileFileRedirectors = [
        /**
         * @todo redirectLogic: Move gfycat ris parameter into config?
         */
        new Redirector("gfycat", /^(.*\/)(?<url>https_thumbs\.gfycat\.com_.*?)(#(?<override_filename>.*))?$/i, gfyCatRedgifRedirectLogic),
        new Redirector("redgifs", /^(.*\/)(?<url>https_thcf\d\.redgifs\.com_.*?)(#(?<override_filename>.*))?$/i, gfyCatRedgifRedirectLogic)
    ];

    possbileFileRedirectors.forEach(redirector => {
        if (redirector.test(document.location.href)) {
            const targetUrl = redirector.redirect(document.location.href);
            if (targetUrl !== undefined) {
                document.location.href = targetUrl;
            }
        }
    });

    /**
     * Redirect logic for both gfycat and redgifs.
     * @param {string} url the URL to apply the redirect logic on
     * @param {RegExp} regex regex to use for extracting the gfycat/redgifs URL.
     *   Requires 2 named groups:
     *     - 'url'
     *     - 'override_filename'
     * @returns the resulting URL
     */
    function gfyCatRedgifRedirectLogic(url, regex) {
        const match = url.match(regex);
        if (match && match.groups) {
            const targetUrl = match.groups["url"].replace(/https?_/, "https://").replace(".com_", ".com/");
            const override = match.groups["override_filename"];
            const gfycatRisParameter = "?ris-goto=0"
            return targetUrl + gfycatRisParameter + "#" + override;
        }
        return undefined;
    }

}