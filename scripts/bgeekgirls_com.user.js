// ==UserScript==
// @name         bgeekgirls.com (ris)
// @version      1.1
// @description  Improved bgeekgirls.com lurking
// @namespace    ris
// @author       (You)
// @match        https://bgeekgirls.com/gallery/*
// @grant        GM_download
// @updateURL    https://bitbucket.org/Rhiki/rhikis-userscripts/raw/master/scripts/bgeekgirls_com.user.js
// @downloadURL  https://bitbucket.org/Rhiki/rhikis-userscripts/raw/master/scripts/bgeekgirls_com.user.js
// @supportURL   https://bitbucket.org/Rhiki/rhikis-userscripts/
// ==/UserScript==

/** TODOs:
 * Proper retry logic
 * Tags as filename placeholders
 */

(function() {
    'use strict';

    // Remove bottom bar
    document.querySelector(".bottom-bar").remove();

    // Download button
    const galleryName = document.querySelector("h1").innerText;
    const downloadTemplate = "bgeekgirls.com/$galleryName/$fileNo - $fileName";
    const progressTemplate = "Total: $total | Completed: $completed | Failed: $failed";
    const progress = { total: 0, completed: 0, failed: 0 };

    const button = document.createElement("button");
    const lblProgress = document.createElement("span");
    button.innerText = "Download gallery";
    button.addEventListener("click", (evt) => {
        const links = [];
        document.querySelectorAll("div.post-content img").forEach(img => links.push(img.src));
        progress.total = links.length;
        progress.completed = 0;
        progress.failed = 0;
        download(links, 0);
    });

    function download(links, index) {
        updateProgress();
        if (index >= links.length) return;
        const dl = {};
        const url = links[index].replace(/-\d+x\d+\./, ".");
        dl.url = url;
        dl.name = downloadTemplate.replace("$galleryName", galleryName)
            .replace("$fileNo", index + 1)
            .replace("$fileName", url.substr(url.lastIndexOf("/") + 1));
        dl.onload = () => {
            progress.completed += 1;
            download(links, index + 1);
        };
        dl.onerror = (error, details) => {
            progress.failed += 1;
            console.log(`Download failed: ${url}. Reason: ${error}`);
            download(links, index + 1);
        };
        dl.ontimeout = () => {
            progress.failed += 1;
            console.log(`Download timed out: ${url}.`);
            download(links, index + 1);
        };

        GM_download(dl);
    }

    function updateProgress() {
        lblProgress.innerText = progressTemplate.replace("$total", progress.total)
            .replace("$completed", progress.completed)
            .replace("$failed", progress.failed);
    }

    document.querySelector("header").appendChild(button);
    document.querySelector("header").appendChild(lblProgress);
})();