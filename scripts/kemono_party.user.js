// ==UserScript==
// @name         kemono.party (ris)
// @version      2.0.0
// @description  Improved kemono.party lurking
// @namespace    ris
// @author       (You)
// @match        https://kemono.party/*/user/*
// @match        https://kemono.party/*/user/*/post/*
// @grant        GM_addStyle
// @grant        GM_openInTab
// @grant        GM_download
// @grant        GM_getValue
// @grant        GM_setValue
// @require      https://bitbucket.org/Rhiki/rhikis-userscripts/raw/download.js-1.1/tools/download.js
// @updateURL    https://bitbucket.org/Rhiki/rhikis-userscripts/raw/master/scripts/kemono_party.user.js
// @downloadURL  https://bitbucket.org/Rhiki/rhikis-userscripts/raw/master/scripts/kemono_party.user.js
// @supportURL   https://bitbucket.org/Rhiki/rhikis-userscripts/
// ==/UserScript==

/**
 * TODOS:
 * - Support for Discord/DlSite
 * - Configurable download template + information about available placeholders
 *   - "$artistName", "Name of the artist"
 *   - "$artistId", "ID of the artist on the respective paywall site"
 *   - "$artist", "Combines and optimizes $artistName and $artistId"
 *   - "$postId", "The ID of the post"
 *   - "$postTitle", "The title/header of this post"
 *   - "$fileNameWithExtension", "The original file name, including extension; for example \"Image_1.png\""
 *   - "$fileName", "The original file name, excluding extension; for example \"Image_1\""
 *   - "$fileExtension", "The file extension of this file, including dot; for example \".png\""
 *   - "$paywall", "The paywall this post is behind"
 *   - "$postDate", "The date of this post, in the format yyyy-MM-dd"
 *   - "$postDateTime", "The date and time of this post in the format yyyyMMddhhmmss"
 */


(function() {

    /**
     * Contains information and configuration for specific paywall sites.
     */
    class Paywall {
        /**
         * Contains information and configuration for specific paywall sites.
         * @param {String} id  the id/name of paywall
         * @param {Function} shouldAddInternalLink
         * function to decide if an (site internal) href should be added to the file list
         * Input is 1 String argument: the href; Output is true or false
         * @param {Function} getPostTitle
         * function to retrieve the title of a post; only applicable when on a post page.
         * No input; returns a string, empty string if unable to retrieve title
         */
        constructor(id, shouldAddInternalLink, getPostTitle) {
            this.id = id;
            this.shouldAddInternalLink = shouldAddInternalLink;
            this.getPostTitle = getPostTitle;
        }
    }

    const Regex = {
        UserInfos: /^https?:\/\/kemono\.party\/(?<paywall>.*?)\/user\/(?<id>.*?)(\?|$)/i,
        PostInfos: /^https?:\/\/kemono\.party\/(?<paywall>.*?)\/user\/(?<artistId>.*?)\/post\/(?<postId>.*)$/i
    }

    const Storage = {
        ArtistDictionary: "artist_dictionary",
        DownloadNameTemplate: "download_name_template"
    }

    const CssClass = {
        SeenPost: "ris-seen-post",
        FilesList: "ris-files-list",
        LinkButton: "ris-link-button",
        DlButton: "ris-dl-button",
        DlInProgress: "ris-dl-in-progress",
        DlSuccess: "ris-dl-success",
        DlFailure: "ris-dl-failure"
    }

    function scrapPostId(href) {
        return href.match(Regex.PostInfos).groups.postId;
    }

    const postInfoMatch = document.location.href.match(Regex.PostInfos);

    if (postInfoMatch != null) {
        postPage(postInfoMatch.groups.paywall, postInfoMatch.groups.artistId, postInfoMatch.groups.postId);
    } else if (Regex.UserInfos.test(document.location.href)) {
        artistPage();
    }

    /**
     * Implementation for post-specific pages.
     * @param {Paywall} paywall which paywall this post is from; handling of various aspects may differ a little
     * @param {String} artistId id of the artist within the paywall, part of the URL
     * @param {String} postId id of the post within the artist/paywall, part of the URL
     */
    function postPage(paywall, artistId, postId) {

        const downloadTemplate = GM_getValue(Storage.DownloadNameTemplate, "kemono.party/$artist/$postId - $postTitle - $fileNameWithExtension");

        /**
         * Loaded from donwloader.js, see https://bitbucket.org/Rhiki/rhikis-userscripts/src/master/tools/download.js
         */
        const downloader = new Downloader(downloadTemplate);

        //#region Metadata and templating

        class PostMetadata {
            constructor(artistId, artistName, postId, postTitle, paywall, postDateTime) {
                this.artistId = artistId;
                this.artistName = artistName;
                this.postId = postId;
                this.postTitle = postTitle;
                this.paywall = paywall;
                this.postDate = postDateTime.substring(0, postDateTime.indexOf(" "));
                this.postDateTime = postDateTime.replaceAll(/[: -]/g, "");
            }

            /**
             * Returns best-effort combination of `artistId` and `artistName`, also handle equal values and absent
             * values.
             * If `artistName` is `undefined` or equal
             * to `artistId`, only `artistid` is returned; otherwise `artistName (artistId)` is returned
             */
            get artist() {
                if (this.artistName === undefined || this.artistId === this.artistName) {
                    return artistId;
                } else {
                    return `${this.artistName} (${this.artistId})`;
                }
            }
        }

        function getMetadata(paywall, artistId, postId) {
            const artistDictionary = GM_getValue(Storage.ArtistDictionary, {});
            const artistKey = `${paywall}_${artistId}`;
            const artistName = artistDictionary[artistKey];
            // Remove the postfix paywall on the title, ie.: "Post Title (Patreon)"
            const postTitle = document.querySelector(".post__title").innerText.replace(/( \(\w*\))$/, "");
            const dateTimeNode = document.querySelector(".subtitle");
            const dateTime = dateTimeNode === null ? "" : dateTimeNode.innerText;

            return new PostMetadata(artistId, artistName, postId, postTitle, paywall, dateTime);
        }

        /**
         * Creates a `TemplateParameters` instance filled with all the static information of this post.
         * @param {PostMetadata} postMetaData
         */
        function createStaticTemplateParameters(postMetaData) {
            return new TemplateParameters()
                .addParameter("$artistName", postMetaData.artistName)
                .addParameter("$artistId", postMetaData.artistId)
                .addParameter("$artist", postMetaData.artist)
                .addParameter("$postId", postMetaData.postId)
                .addParameter("$postTitle", postMetaData.postTitle.replaceAll(/\//g, "-"))
                .addParameter("$paywall", postMetaData.paywall)
                .addParameter("$postDate", postMetaData.postDate)
                .addParameter("$postDateTime", postMetaData.postDateTime);
        }

        /**
         * Creates a copy of the passed `staticParameters`, and fills in file-specific parameters
         * @param {TemplateParameters} staticParameters TemplateParameters instance filled with static information
         * about this post.
         * @param {PostFile} file the file, which info will be inserted.
         */
        function fillFileParameters(staticParameters, file) {
            const params = new TemplateParameters()
                .addParameter("$fileNameWithExtension", file.fileNameWithExtension)
                .addParameter("$fileName", file.fileNameWithoutExtension)
                .addParameter("$fileExtension", file.fileExtension);
            staticParameters.forEach((key, value) => params.addParameter(key, value));
            return params;
        }

        const metadata = getMetadata(paywall, artistId, postId);
        const staticTempalteParameters = createStaticTemplateParameters(metadata);

        //#endregion

        class PostLinks {
            /**
             * Container for all (valid) links on this page
             * @param {Array<PostFile>} internal array of links to the kemono.party domain
             * @param {Array<PostFile>} external array of links to any external domain
             */
            constructor(internal, external) {
                this.internal = internal;
                this.external = external;
            }
        }
        class PostFile {
            /**
             *
             * @param {String} href the location of the file
             * @param {String} fileName the name of the file, including file extension
             */
            constructor(href, fileName) {
                this.href = href;
                this.fileName = fileName;
            }

            get fileNameWithExtension() {
                return this.fileName;
            }

            get fileNameWithoutExtension() {
                return this.fileName.substring(0, this.fileName.lastIndexOf("."));
            }

            get fileExtension() {
                return this.fileName.substr(this.fileName.lastIndexOf("."));
            }
        }
        GM_addStyle(`ul.${CssClass.FilesList} { list-style: none; margin: 0; padding: 0; }`);
        GM_addStyle(`ul.${CssClass.FilesList} li.${CssClass.DlInProgress} * { color: lightyellow; }`);
        GM_addStyle(`ul.${CssClass.FilesList} li.${CssClass.DlSuccess} * { color: lightgreen; }`);
        GM_addStyle(`ul.${CssClass.FilesList} li.${CssClass.DlFailure} * { color: red; }`);
        GM_addStyle(`.${CssClass.LinkButton} { cursor: pointer; padding-right: 4px; }`);


        createUserscriptUi(document.querySelector("div.post__body"), getFiles());

        /**
         * Creates the userscript UI.
         * @param {Node} appendTarget the node to append the download panel to
         * @param {PostLinks} links links to be displayed in the download panel
         */
        function createUserscriptUi(appendTarget, links) {
            const container = document.createElement("div");
            container.style.border = "solid rgba(128,128,128,.7) .125em"; //to get the border the page is using for its post page sections
            createUserScriptSection(container, links);
            createLinkSection("Files:", links.internal, container, true);
            createLinkSection("External links:", links.external, container, false);
            appendTarget.parentNode.insertBefore(container, appendTarget);

            /**
             * Creates the user script section of the UI.
             * @param {Node} container the container to append the created UI elements to
             */
            function createUserScriptSection(container, links) {
                const header = document.createElement("h2");
                header.innerText = "Userscript";
                container.appendChild(header);
                const openAll = document.createElement("a");
                openAll.classList.add(CssClass.LinkButton);
                openAll.innerText = "[ Open all links in new tab ]";
                openAll.addEventListener("click", (evt) => {
                    evt.preventDefault();
                    links.internal.reverse().forEach(link => GM_openInTab(link.href, true));
                    links.external.reverse().forEach(link => GM_openInTab(link.href, true));
                });
                container.appendChild(openAll);
            }

            /**
             * Creates a link section UI.
             * @param {String} headerText the text for the header
             * @param {Array<PostFile>} links the links to display in this section
             * @param {Node} appendTarget the node to append created elements to
             * @param {boolean} includeDlButton Optional; Whether or not a download button should be prefixed to each link
             */
            function createLinkSection(headerText, links, appendTarget, includeDlButton) {
                const h2 = document.createElement("h2");
                const list = document.createElement("ul");
                h2.innerText = headerText;
                appendTarget.appendChild(h2);
                if (includeDlButton) {
                    const dlButton = document.createElement("a");
                    dlButton.innerText = "[ Download all files ]";
                    dlButton.classList.add(CssClass.LinkButton);
                    dlButton.addEventListener("click", (evt) => {
                        downloader.enableQueue();
                        list.querySelectorAll(`.${CssClass.DlButton}`).forEach(btn => btn.click());
                        downloader.disableQueue();
                        downloader.downloadQueue();
                    });
                    appendTarget.appendChild(dlButton);
                }
                list.classList.add(CssClass.FilesList);
                links.forEach(link => {
                    const li = document.createElement("li");
                    if (includeDlButton) {
                        const dl = document.createElement("a");
                        dl.innerText = "[DL]";
                        dl.classList.add(CssClass.LinkButton, CssClass.DlButton);
                        dl.addEventListener("click", (evt) => {
                            evt.preventDefault();
                            // Remove previous success/failure states
                            li.classList.remove(CssClass.DlFailure, CssClass.DlSuccess);
                            const params = fillFileParameters(staticTempalteParameters, link);
                            li.classList.add(CssClass.DlInProgress);
                            downloader.downloadWithTemplate(link.href, params, () => {
                                li.classList.add(CssClass.DlSuccess);
                                li.classList.remove(CssClass.DlInProgress);
                            }, (e) => {
                                li.classList.add(CssClass.DlFailure);
                                li.classList.remove(CssClass.DlInProgress);
                                console.log(e);
                            });
                        });
                        li.appendChild(dl);
                    }
                    const a = document.createElement("a");
                    a.href = link.href;
                    a.target = "_blank";
                    a.innerText = link.fileName;
                    li.appendChild(a);
                    list.appendChild(li);
                });
                appendTarget.appendChild(list);
            }
        }

        /**
         * Retrieves all downloadable files of the post.
         * @returns {PostLinks} an object containing all valid internal and external links on the current page.
         */
        function getFiles() {
            const links = new PostLinks([], [])
            document.querySelectorAll("div.post__body a").forEach(a => {
                if (a.hostname === "kemono.party") {
                    let fileName = a.href.substr(a.href.lastIndexOf("/") + 1)
                    if (fileName.includes("?f=")) {
                        fileName = fileName.substr(fileName.indexOf("?f=") + "?f=".length);
                    }
                    if (fileName.endsWith(".jpe")) {
                        fileName = fileName.replace(/\.jpe$/i, ".jpg");
                    }
                    if (!links.internal.find((l) => l.href === a.href)) {
                        // Pages with a preview image often have the same image/link in the post body again.
                        // Prevent adding multiples of the same link here.
                        links.internal.push(new PostFile(a.href, decodeURIComponent(fileName)));
                    }
                } else if (a.hostname !== "") { // filter javascript: links, ie favorite
                    links.external.push(new PostFile(a.href, a.href));
                }
            });
            return links;
        }
    }

    /**
     * Implementation of artist pages.
     */
    function artistPage() {

        /**
         * @param paywall Which paywall website is used, can be found as the first directory of the URL
         * @param id The id
         */
        class ArtistInfo {
            constructor(paywall, id, name) {
                this.paywall = paywall;
                this.id = id;
                this.name = name;
            }
        }

        GM_addStyle(`.${CssClass.SeenPost} { border: 2px dotted orange; }`);

        const artistMetaData = scrapArtistInfo();
        const artistkey = `${artistMetaData.paywall}_${artistMetaData.id}`;
        updateArtistDictioniary();
        handleSeenPosts();

        /**
         * Updates the persisted artist id => artist name dictionary, that is used when downloading files.
         */
        function updateArtistDictioniary() {
            const artistDictionary = GM_getValue(Storage.ArtistDictionary, {});
            artistDictionary[artistkey] = artistMetaData.name;
            GM_setValue(Storage.ArtistDictionary, artistDictionary);
        }

        function handleSeenPosts() {
            const seenPostsKey = `seen_posts_${artistkey}`;
            const seenPosts = GM_getValue(seenPostsKey, []);
            const seenTotal = seenPosts.length;

            document.querySelectorAll(".card-list__items article").forEach(post => {
                const postId = scrapPostId(post.querySelector(".post-card__link a:first-child").href);
                if (seenPosts.includes(postId)) {
                    post.classList.add(CssClass.SeenPost);
                } else {
                    seenPosts.push(postId);
                }
            });
            GM_setValue(seenPostsKey, seenPosts);
            const seenOnPage = document.querySelectorAll(`.${CssClass.SeenPost}`).length;
            document.querySelectorAll(".paginator > small").forEach(paginator => {
                paginator.innerText = `${paginator.innerText} (Seen: ${seenOnPage} on page, ${seenTotal} total)`;
            })
        }

        function scrapArtistInfo() {
            const href = document.location.href;
            const match = href.match(Regex.UserInfos);
            const paywall = match.groups.paywall;
            const id = match.groups.id;
            const name = document.querySelector(".user-header__profile > span:last-child").innerText;

            return new ArtistInfo(paywall, id, name);
        }
    }
})();