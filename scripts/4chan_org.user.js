// ==UserScript==
// @name         4chan.org + 4chan X + archives (ris)
// @description  Improve 4chan lurking, requires 4chan X to work properly.
// @version      1.2.1
// @namespace    ris
// @author       (You)
// @match        https://boards.4chan.org/*/thread/*
// @match        https://boards.4channel.org/*/thread/*
// @grant        GM_setClipboard
// @grant        GM_download
// @grant        GM_addStyle
// @updateURL    https://bitbucket.org/Rhiki/rhikis-userscripts/raw/master/scripts/4chan_org.user.js
// @downloadURL  https://bitbucket.org/Rhiki/rhikis-userscripts/raw/master/scripts/4chan_org.user.js
// @supportURL   https://bitbucket.org/Rhiki/rhikis-userscripts/
// ==/UserScript==


/**
 * @todo Maybe: Overwrite 4chan X default download behaviour on click
 * @todo Download full thread
 * @todo Link list: collapsible
 * @todo Customizable user settings
 */
(function() {
    "use strict";

    //#region User settings

    const DEFAULT_FILENAME_TEMPLATE = "4chan/$board/$threadTitle ($threadDate)/$fileName";

    class Settings {
        /**
         * @param {boolean} galleryExtensionsEnabled Whether the gallery extensions are enabled, or not
         * @param {boolean} galleryCopyOnSave Whether the link to the file is copied to the clipboard when saving, or not
         * @param {String} fileNameTemplate The filename tempalte to be used when saving files.
         * @param {boolean} linkListEnabled Whether the link list should be displayed, or not
         */
        constructor(galleryExtensionsEnabled, galleryCopyOnSave, fileNameTemplate, linkListEnabled) {
            // 4chan X gallery extensions
            this.galleryExtensionsEnabled = galleryExtensionsEnabled;
            this.galleryCopyOnSave = galleryCopyOnSave;
            this.fileNameTemplate = fileNameTemplate;
            // link list
            this.linkListEnabled = linkListEnabled;
        }
    }

    /**
     * Load the persisted user settings, or use default settings if none were saved.
     * @returns {Settings} the settings
     * @todo persisting settings and allowing the user to change settings.
     */
    function loadUserSettings() {
        return new Settings(true, true, DEFAULT_FILENAME_TEMPLATE, true);
    }

    //#endregion

    //#region Meta info

    class MetaInfo {
        /**
         * 
         * @param {String} boardAbbreviation board abbreviation, ie.: "a", "vrpg"
         * @param {String} boardTitle full board title, ie.: "Anime & Manga", "Video Games RPG"
         * @param {String} threadTitle title of the thread
         * @param {String} threadDate UTC timestamp of the OP post
         */
        constructor(boardAbbreviation, boardTitle, threadTitle, threadDate) {
            this.boardAbbreviation = boardAbbreviation;
            this.boardTitle = boardTitle;
            this.threadTitle = threadTitle;
            this.threadDate = threadDate;
        }
    }

    /**
     * Extract meta data from the current thread such as current board, threat title, etc.
     */
    function extractMetaData() {
        const titleRegex = /(?<unread_post_count>\(\d+\) )?\/(?<board>.+?)\/ - (?<thread_title>.*) - (?<board_name>.*) - 4chan/i;
        const titleGroups = document.title.match(titleRegex).groups;
        const board = titleGroups.board;
        const threadTitle = titleGroups.thread_title;
        const threadDate = document.querySelector("div.post.op span.dateTime").getAttribute("data-utc");
        return new MetaInfo(board, getBoardTitle(board), threadTitle, threadDate);
    }

    /**
     * Get the board's full title from it's abbreviation
     * @param {String} board board abbreviation
     * @returns the board's full title
     * @todo implement me
     */
    function getBoardTitle(board) {
        return board;
    }
    //#endregion

    //#region 4chan X gallery extensions
    const galleryExtensionModule = function() {

        const downloadCache = [];

        const galleryKeybinds = (evt) => {
            var key = evt.keyCode || evt.which;
            switch (evt.code) {
                case "Numpad5":
                case "KeyS":
                case "ArrowDown":
                    const dl = document.querySelector(".gal-name");
                    download(dl.href, dl.innerText);
                    if (settings.galleryCopyOnSave) GM_setClipboard(dl.href);
                    break;
                case "KeyA":
                    document.querySelector(".gal-prev").click();
                    break;
                case "KeyC":
                    GM_setClipboard(document.querySelector(".gal-name").href);
                    break;
                case "KeyD":
                    document.querySelector(".gal-next").click();
                    break;
            }
        };

        const galleryObserver = new MutationObserver((mutationsList, observer) => {
            mutationsList.forEach(mutation => {
                if (mutation.addedNodes.length > 0 && mutation.addedNodes[0].id === "a-gallery") {
                    document.body.addEventListener("keydown", galleryKeybinds);
                    downloadCache.forEach(cached => {
                        const thumb = document.querySelector(`#a-gallery .gal-thumb[href="${cached}"]`);
                        if (thumb != null) {
                            thumb.style.border = "2px solid green";
                        }
                    });
                } else if (mutation.removedNodes.length > 0 && mutation.removedNodes[0].id === "a-gallery") {
                    document.body.removeEventListener("keydown", galleryKeybinds);
                }
            });
        });

        function download(href, fileName) {
            const fileTimestamp = href.substring(href.lastIndexOf("/") + 1, href.lastIndexOf("."));
            const downloadFileName = fillTemplate(fileName, fileTimestamp);
            const details = {
                url: href,
                name: downloadFileName,
                onload: () => {
                    document.querySelector(".gal-highlight").style.border = "2px solid green";
                    downloadCache.push(href);
                },
                onerror: (download) => {
                    console.log(`Download failed: ${href} => ${downloadFileName}. Reason: ${download.error}`);
                    document.querySelector(".gal-highlight").style.border = "2px solid red";
                },
                ontimeout: () => {
                    console.log(`Download timed out: ${href} => ${downloadFileName}.`);
                    document.querySelector(".gal-highlight").style.border = "2px solid red";
                }
            };
            GM_download(details);
        }

        function fillTemplate(fileName, fileTimestamp) {
            return settings.fileNameTemplate.replace("$board", metaInfo.boardAbbreviation)
                .replace("$boardTitle", metaInfo.boardTitle)
                .replace("$threadTitle", metaInfo.threadTitle.replace(/[\/\\\.]/g, "_"))
                .replace("$threadDate", metaInfo.threadDate)
                .replace("$fileName", fileName)
                .replace("$fileTimestamp", fileTimestamp);
        }

        const module = {};
        module.enable = () => { galleryObserver.observe(document.body, { childList: true, subtree: true }) };
        module.disable = () => { galleryObserver.disconnect(); };
        return module;
    }();

    //#endregion

    //#region link list 
    const linkListModule = function() {

        const linkList = document.createElement("ul");
        linkList.id = "ris_linklist";

        function addStyles() {
            GM_addStyle(`
                ul#ris_linklist {
                    position: fixed;
                    top: 20px;
                    right: 15px;
                    background: rgba(0,0,0,0.3);
                    padding: 5px;
                    list-style: none;
                    max-width: 400px;
                    white-space: nowrap;
                    overflow-x: hidden;
                    overflow-y: auto;
                    max-height: 90vh;
                    text-overflow: ellipsis;
                }

                .ris-jump-to-post {
                    padding-right: 5px;
                    cursor: pointer; 
                }

                #ris_linklist a:visited {
                    color: lightgreen !important;
                }
            `);
        }

        function clearLinkList() {
            while (linkList.firstChild) { linkList.firstChild.remove(); }
        }

        function fillLinkList() {
            // clear out first
            clearLinkList();

            linkList.appendChild(createButtons());

            document.querySelectorAll("a.linkify").forEach(a => {
                const li = document.createElement("li");

                const jumpToPost = document.createElement("a");
                jumpToPost.innerText = ">>";
                jumpToPost.classList.add("ris-jump-to-post")
                jumpToPost.addEventListener("click", (evt) => {
                    evt.preventDefault();
                    a.scrollIntoView({ block: "center", behavior: "smooth" });
                });
                li.appendChild(jumpToPost);

                const copy = document.createElement("a");
                copy.href = a.href;
                copy.innerText = a.innerText;
                li.appendChild(copy);
                linkList.appendChild(li);
            });
        }

        function createButtons() {
            const liButtons = document.createElement("li");
            const refresh = document.createElement("button");
            refresh.innerText = "Refresh";
            refresh.addEventListener("click", (evt) => fillLinkList());
            liButtons.appendChild(refresh);
            const collapse = document.createElement("button");
            collapse.innerText = "Collapse";
            collapse.addEventListener("click", (evt) => {
                clearLinkList();
                const expand = document.createElement("button");
                expand.innerText = "Expand";
                expand.addEventListener("click", (evt) => {
                    fillLinkList();
                });
                linkList.appendChild(expand);
            });
            liButtons.appendChild(collapse);
            return liButtons;
        }

        const module = {};

        module.enable = () => {
            addStyles();
            fillLinkList();
            document.body.appendChild(linkList);
        };
        module.disable = () => {
            linkList.remove();
        };

        return module;
    }();
    //#endregion

    // Initialization
    const metaInfo = extractMetaData();
    const settings = loadUserSettings();

    if (settings.galleryExtensionsEnabled) {
        galleryExtensionModule.enable();
    }
    if (settings.linkListEnabled) {
        linkListModule.enable();
    }
})();