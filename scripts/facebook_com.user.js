// ==UserScript==
// @name         facebook.com (ris)
// @version      1.1
// @description  Improve Facebook lurking
// @namespace    ris
// @author       (You)
// @match        https://www.facebook.com
// @grant        none
// @updateURL    https://bitbucket.org/Rhiki/rhikis-userscripts/raw/master/scripts/facebook_com.user.js
// @downloadURL  https://bitbucket.org/Rhiki/rhikis-userscripts/raw/master/scripts/facebook_com.user.js
// @supportURL   https://bitbucket.org/Rhiki/rhikis-userscripts/
// ==/UserScript==

(function() {
    'use strict';

    const language = document.documentElement.lang;
    /**
     * If you are using Facebook in a language different than English, you'll have to add your language's "Suggested for you" text here
     * (below the `en: "Suggested for You",` line).
     * If you need any help or want to add your translation, create a ticket at https://bitbucket.org/Rhiki/rhikis-userscripts/issues/new
     */
    const suggestedForYouI18n = {
        en: "Suggested for You",
    }

    const suggestedForYouString = suggestedForYouI18n[language];


    removePosts(true, true);

    function removePosts(sponsored, suggested) {
        const observer = new MutationObserver((mutations, observer) => {
            mutations.forEach(mutation => {
                if (mutation.addedNodes) {
                    mutation.addedNodes.forEach(addedNode => {
                        if (addedNode.tagName === "DIV" && addedNode.getAttribute("data-pagelet") !== null) {
                            handlePost(addedNode);
                            return;
                        }
                    });
                }
            });
        });
        const target = document.body.querySelector("[role='feed']") || document.body;
        observer.observe(target, { attributes: false, childList: true, subtree: true });

        // Iterate over all posts that exist at script execution
        document.querySelectorAll("div[data-pagelet]").forEach(post => handlePost(post));

        function handlePost(post) {
            if (sponsored && isSponsored(post)) {
                post.parentElement.removeChild(post);
                console.log("facebook.com (ris): Removed sponsored post!");
            } else if (suggested && isSuggestedForYou(post)) {
                post.parentElement.removeChild(post);
                console.log("facebook.com (ris): Removed suggested post!");
            }
        }

        function isSponsored(post) {
            return post !== undefined && post.querySelector("[aria-label='Sponsored']") !== null;
        }

        function isSuggestedForYou(post) {
            if (post !== undefined) {
                const suggested = post.querySelector("span:only-child");
                return suggested != null && suggested.textContent === suggestedForYouString;
            }
            return false;
        }
    }
})();