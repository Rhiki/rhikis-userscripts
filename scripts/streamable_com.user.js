// ==UserScript==
// @name         streamable.com (ris)
// @description  Add a download button to streamable videos.
// @version      1.0
// @namespace    ris
// @author       (You)
// @match        https://streamable.com/*
// @grant        GM_download
// @updateURL    https://bitbucket.org/Rhiki/rhikis-userscripts/raw/master/scripts/streamable_com.user.js
// @downloadURL  https://bitbucket.org/Rhiki/rhikis-userscripts/raw/master/scripts/streamable_com.user.js
// @supportURL   https://bitbucket.org/Rhiki/rhikis-userscripts/
// ==/UserScript==

(function() {
    'use strict';
    const btnDownload = document.createElement("button");
    btnDownload.innerText = "Download video";
    btnDownload.addEventListener("click", (evt) => {
        const src = document.querySelector("video").src;
        GM_download(src, document.querySelector("#title").innerText + getFileExtension(src));
    });
    document.querySelector(".actions-section").appendChild(btnDownload);

    function getFileExtension(src) {
        const extensionEnd = src.indexOf("?");
        return src.substring(src.lastIndexOf("."), extensionEnd > 0 ? extensionEnd : src.length);
    }
})();