// ==UserScript==
// @name         dropbox.com (ris)
// @version      1.0.0
// @description  Improved Dropbox browsing
// @namespace    ris
// @author       (You)
// @include      /^https?:\/\/(www\.)?dropbox\.com\/.*$/
// @grant        GM_addStyle
// @grant        GM_download
// @grant        GM_setClipboard
// @run-at       document-idle
// ==/UserScript==

(() => {
    "use strict";

    // Hide an annoying footer
    GM_addStyle(".persistent-footer { display: none !important }");

    // Add style rules for modal dialogs.
    GM_addStyle(`
        #ris-modal {
            z-index: 1000;
            position: fixed;
            left: 0;
            top: 0;
            width: 100vw;
            height: 100vh;
            overflow: auto;
            background-color: rgba(0,0,0,0.4);
        }
        #ris-modal-content {
            background-color: #eee;
            margin: 15% auto;
            padding: 20px;
            border: 1px solid 333;
            width: 80%;
        }
    `);

    // Add styles for gallery downloads.
    GM_addStyle(`
        .ris-download-success, .ris-download-failure, .ris-download-ongoing {
            border-radius: 5px;
            border-width: 2px;
            border-style: dotted;
        }

        .ris-download-success {
            border-color: green;
        }
        .ris-download-failure {
            border-color: red;
        }
        .ris-download-ongoing {
            border-color: purple;
        }
    `);

    document.body.addEventListener("keydown", (event) => {
        const key = event.keyCode || event.which;
        if (key == 83 || key == 12) { // 's' or numpad 5 (no lock)
            if (!downloadViaButton()) {
                downloadViaUserscript();
            }
        }
    });

    /**
     * Download via the normal "Download"-button provided by Dropbox.
     * @returns true if download was successful; false otherwise: most likely means that
     * downloading is disallowed by the uploader.
     */
    function downloadViaButton() {
        const downloadButton = document.querySelector(".download-button:not(.mc-button-disabled)");
        if (downloadButton !== null) {
            downloadButton.click();
            document.querySelector("span.mc-popover-content-item").click();
            return true;
        }
        return false;
    }

    /**
     * Download with GM_download(). Use to workaround prohibited downloads.
     * @returns true if a download was successfully initiated; false otherwise
     */
    function downloadViaUserscript() {
        const gallery = document.querySelector(".sl-grid-body");
        if (gallery !== null) {
            return downloadGallery();
        }

        const imagePreview = document.querySelector(".preview-type-image");
        if (imagePreview !== null) {
            return downloadSingleImage();
        }

        const videoPlayer = document.querySelector(".video-player");
        if (videoPlayer !== null) {
            return downloadSingleVideo();
        }

        /**
         * Downloads the previewed image with the original file name.
         * Regardless of which quality/size the image is displayed, will always download original size.
         */
        function downloadSingleImage() {
            const image = document.querySelector(".preview-type-image .preview-image img");
            if (image !== null) {
                const url = fixImageSize(img.src);
                const name = document.querySelector(".filename--text").innerText;
                GM_download(url, name);
                return true;
            }
            return false;
        }

        function fixImageSize(src) {
            // Replace any size parameters and set size_mode =5, which should be original size
            return src.replace(/\?.*$/, "?size_mode=5");
        }

        /**
         * Generates an ffmpeg script to download the previewed video with the original file name.
         * It's not ideal, but Dropbox provides video source only as m3u8 file so that's the simplest solution.
         */
        async function downloadSingleVideo() {
            const M3U8_STREAM_INF = "#EXT-X-STREAM-INF:";

            class AvailableSource {
                /**
                 * @param {string} info String containing information about this source (resolution, bitrate, etc.)
                 * @param {string} link link to the video stream
                 */
                constructor(info, link) {
                    this.info = info;
                    this.link = link;
                }

                toString() {
                    let result = "Source {";
                    Object.keys(this.info).forEach(key => {
                        result += ` ${key}=${this.info[key]} `;
                    });
                    return result + "}"
                }

                toFfmpegScript(fileName) {
                    return `ffmpeg -i "${this.link}" -vcodec copy "${fileName}"`;
                }
            }

            const videoSource = document.querySelector("video > source");
            if (videoSource !== null) {
                const response = await fetch(videoSource.src);
                const m3u8 = (await response.text()).split("\n");
                const availableSources = [];
                for (let i = 0; i < m3u8.length; i++) {
                    const currentLine = m3u8[i];
                    if (currentLine.startsWith(M3U8_STREAM_INF)) {
                        const information = splitM3u8StreamInf(currentLine);
                        const link = m3u8[++i];
                        availableSources.push(new AvailableSource(information, link));
                    }
                }
                showModalDialog(availableSources);
            }

            function splitM3u8StreamInf(streamInf) {
                const result = {};
                const keyValues = streamInf.replace(M3U8_STREAM_INF, "") // Remove start of tag
                    .replaceAll(", ", " ") // Replace value-array separators, so the split on "," will work correctly
                    .replaceAll("\"", "") // Remove all extra quotation marks (value arrays)
                    .split(","); // Split into key-value pairs, separated by "="
                keyValues.forEach(keyValue => {
                    const [key, value] = keyValue.split("=", 2);
                    result[key] = value;
                });
                return result;
            }

            function showModalDialog(availableSources) {
                const modal = document.querySelector("#ris-modal");
                if (modal !== null) {
                    document.body.removeChild(modal);
                }
                createModalDialog(availableSources);
            }

            /**
             * Display a modal dialog for copying the video source URLs.
             * @param {AvailableSource} availableSources the availabe video source to display
             */
            function createModalDialog(availableSources) {
                const fileName = document.querySelector(".filename--text").innerText;
                const modal = createModalContainer();
                const content = createModalContent();
                availableSources.forEach(source => content.appendChild(createSourceRow(source, fileName)));
                modal.appendChild(content);
                document.body.appendChild(modal);


                function createModalContainer() {
                    const modal = document.createElement("div");
                    modal.id = "ris-modal";
                    modal.addEventListener("click", evt => {
                        if (evt.target === modal) {
                            document.body.removeChild(modal);
                        }
                    });
                    return modal;
                }

                function createModalContent() {
                    const content = document.createElement("div");
                    content.id = "ris-modal-content";
                    return content;
                }

                function createSourceRow(source, fileName) {
                    const sourceInfo = document.createElement("p");
                    sourceInfo.innerText = source.toString();

                    const copyButton = document.createElement("button");
                    copyButton.innerText = "📋";
                    copyButton.addEventListener("click", () => GM_setClipboard(ffmpegScript.value));

                    const ffmpegScript = document.createElement("input");
                    ffmpegScript.value = source.toFfmpegScript(fileName);

                    const container = document.createElement("div");
                    container.appendChild(sourceInfo);
                    container.appendChild(copyButton);
                    container.appendChild(ffmpegScript);
                    container.appendChild(document.createElement("hr"));
                    return container;
                }
            }
        }

        function downloadGallery() {

            if (isDownloadInProgress()) {
                return;
            }

            let downloadStatus = document.querySelector("#ris-download-status");
            if (downloadStatus === null) {
                downloadStatus = document.createElement("span");
                downloadStatus.id = "ris-download-status"
                downloadStatus.style.margin = "10px";
                document.querySelector(".breadcrumb-trail").appendChild(downloadStatus);
            }

            const gridCells = document.querySelectorAll(".sl-grid-body .sl-grid-cell:not(.ris-download-success)");
            const totalDownloads = document.querySelectorAll(".sl-grid-body .sl-grid-cell").length;
            updateDownloadStatus();
            const galleryName = document.querySelector(".breadcrumb-segment").innerText;
            gridCells.forEach(gridCell => {
                try {
                    setCellDownloadStatus(gridCell, true, false, false);
                    const image = gridCell.querySelector("img");
                    const url = fixImageSize(image.src);
                    const name = `[${galleryName}] ${image.alt}`;
                    const onError = (download) => {
                        console.log(`Download for '${name}' failed.`);
                        console.log(download);
                        setCellDownloadStatus(gridCell, false, false, true);
                        updateDownloadStatus();
                    };
                    const onSuccess = () => {
                        setCellDownloadStatus(gridCell, false, true, false);
                        updateDownloadStatus();
                    };
                    GM_download({ url: url, name: name, onerror: onError, ontimeout: onError, onload: onSuccess });
                } catch (error) {
                    setCellDownloadStatus(gridCell, false, false, true);
                }

            });
            return false;

            function isDownloadInProgress() {
                return document.querySelectorAll(".ris-download-ongoing").length > 0;
            }

            function updateDownloadStatus() {
                const success = document.querySelectorAll(".ris-download-success").length;
                const failed = document.querySelectorAll(".ris-download-failure").length;
                const percentage = Math.round(((success + failed) / totalDownloads) * 100);
                downloadStatus.innerText = `Downloading: S ${success} | F ${failed} | T ${totalDownloads} (${percentage}%)`;
            }

            function setCellDownloadStatus(cell, ongoing, success, failure) {
                cell.classList.toggle("ris-download-ongoing", ongoing);
                cell.classList.toggle("ris-download-success", success);
                cell.classList.toggle("ris-download-failure", failure);
            }
        }
    }
})();