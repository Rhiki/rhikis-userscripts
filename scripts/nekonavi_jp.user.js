// ==UserScript==
// @name         nekonavi.jp (ris)
// @version      1.0
// @description  Add an image download button for nekonavi.jp/catblog posts.
// @namespace    ris
// @author       (You)
// @match        https://nekonavi.jp/catblog/archives/*
// @grant        GM_download
// @updateURL    https://bitbucket.org/Rhiki/rhikis-userscripts/raw/master/scripts/nekonavi_jp.user.js
// @downloadURL  https://bitbucket.org/Rhiki/rhikis-userscripts/raw/master/scripts/nekonavi_jp.user.js
// @supportURL   https://bitbucket.org/Rhiki/rhikis-userscripts/
// ==/UserScript==

(function () {
    'use strict';

    const blogPostTitle = document.querySelector("h1.entry-title").innerText;
    const blogAuthor = document.querySelector(".profile-name").innerText;
    const blogPostDate = new Date(document.querySelector("time.entry-date").dateTime).toISOString().replace(/T.+/g, "");
    const downloadFolder = "nekonavi.jp";

    // Top
    document.querySelector(".sns-top > ol").appendChild(createDownloadListItem(createDownloadAnchor()));
    // Bottom
    document.querySelector(".sns > ol").appendChild(createDownloadListItem(createDownloadAnchor()));

    function createDownloadListItem(downloadAnchor) {
        const li = document.createElement("li");
        li.appendChild(downloadAnchor);
        return li;
    }

    function createDownloadAnchor() {
        const a = document.createElement("a");
        a.innerText = "💾 Download";
        a.addEventListener("click", downloadComic);
        a.style.color = "#333";
        a.style.border = "1px solid #333";
        a.style.cursor = "pointer";
        return a;
    }

    function downloadComic(event) {
        if (event !== undefined) event.preventDefault();
        const imgs = document.querySelectorAll("img[class*='wp-image']");
        for (let i = 0; i < imgs.length; i++) {
            const img = imgs[i].src;
            const ext = img.substr(img.lastIndexOf(".") + 1);
            const source = getFullSizeImageSource(img);
            const fileName = `${downloadFolder}/${blogAuthor} - ${blogPostTitle} - page ${i + 1} (${blogPostDate}).${ext}`;
            GM_download(source, fileName);
        }
    }

    /**
     * Replace resize parameter at end of the file name,
     * for example:
     * https://nekonavi.jp/wp-content/uploads/2020/05/A7418E57-7787-46C0-8ACB-3F22F49E6E32-640x360.png
     * turns into:
     * https://nekonavi.jp/wp-content/uploads/2020/05/A7418E57-7787-46C0-8ACB-3F22F49E6E32.png
     * @param {string} imgSrc URL to image source
     * @returns {string} URL with resize parameters removed
     */
    function getFullSizeImageSource(imgSrc) {
        return imgSrc.replace(/-\d+x\d+\./, ".");;
    }
})();