// ==UserScript==
// @name         slushe.com (ris)
// @version      1.2.0
// @description  Improve slush.com lurking
// @author       (You)
// @namespace    ris
// @match        https://slushe.com/galleries/*
// @match        https://slushe.com/video/*
// @require      https://bitbucket.org/Rhiki/rhikis-userscripts/raw/download.js-1.0.1/tools/download.js
// @grant        GM_download
// @grant        GM_setClipboard
// @grant        GM_openInTab
// @grant        GM_getValue
// @grant        GM_setValue
// @grant        GM_addValueChangeListener
// ==/UserScript==

(function() {
    'use strict';

    //#region constants, classes
    const FG_COLOR = "#ff00ba";
    const BG_COLOR = "#000";
    const FILE_NAME_TEMPLATE_KEY = "FILE_NAME_TEMPLATE_KEY";
    const DEFAULT_FILE_NAME_TEMPLATE = "$artist ($uploadDate) $galleryName {$tags}";
    const COPY_URL_ON_DOWNLOAD_KEY = "COPY_URL_ON_DOWNLOAD_KEY";
    const FAV_ON_DOWNLOAD_KEY = "FAV_ON_DOWNLOAD_KEY";
    const LIKE_ON_DOWNLOAD_KEY = "LIKE_ON_DOWNLOAD_KEY";

    /**
     * Meta information about the gallery
     */
    class GalleryInfo {
        /**
         * @param {String} artist Name of the artist/uploader
         * @param {String} uploadDate Human readable string representation of the upload date
         * @param {String} galleryName Name of the gallery
         * @param {String} tags String concatenated representation of the tags of the gallery
         */
        constructor(artist, uploadDate, galleryName, tags) {
            this.artist = artist;
            this.uploadDate = uploadDate;
            this.galleryName = galleryName;
            this.tags = tags;
        }
    };
    //#endregion

    //#region initalization

    // No need to set a template as fillTemplate is already implemented for this script so no need to refactor it all.
    const downloader = new Downloader();

    let copyUrlOnDownload = loadAndHookupSetting(COPY_URL_ON_DOWNLOAD_KEY, false, (newValue) => copyUrlOnDownload = newValue);
    let favOnDownload = loadAndHookupSetting(FAV_ON_DOWNLOAD_KEY, false, (newValue) => favOnDownload = newValue);
    let likeOnDownload = loadAndHookupSetting(LIKE_ON_DOWNLOAD_KEY, false, (newValue) => likeOnDownload = newValue);
    let fileNameTemplate = loadAndHookupSetting(FILE_NAME_TEMPLATE_KEY, DEFAULT_FILE_NAME_TEMPLATE, (newValue) => likeOnDownload = newValue);

    function loadAndHookupSetting(key, defaultValue, onRemoteUpdate) {
        GM_addValueChangeListener(key, (name, oldValue, newValue, remote) => {
            if (remote) onRemoteUpdate(newValue);
        });
        return GM_getValue(key, defaultValue);
    }
    const metaData = extractMetadata();

    // UI manipulation
    const buttonAppendTarget = document.querySelector("main > ul");
    const btnDownload = createButton("[S] Download", evt => {
        clickButton("like", likeOnDownload);
        clickButton("favorite", favOnDownload);
        allMedia(download);

        if (copyUrlOnDownload) {
            GM_setClipboard(document.location.href); // Hotlinking returns 403
        }

        function clickButton(type, isEnabled) {
            if (isEnabled && document.querySelector(".user-menu") !== null) { // requires login
                const button = document.querySelector(`.buttons > .${type}:not(.active)`);
                if (button !== null) button.click();
            }
        }
    });
    const btnOpenInNewTab = createButton("Open in new tab", evt => allMedia(openInNewTab, true));
    const btnSettings = createButton("Userscript settings", evt => openSettingsDialog());
    buttonAppendTarget.appendChild(btnDownload);
    buttonAppendTarget.appendChild(btnOpenInNewTab);
    buttonAppendTarget.appendChild(btnSettings);

    document.addEventListener("keypress", evt => {
        switch (evt.code) {
            case "KeyS":
                btnDownload.click();
                break;
        }
    });

    //#endregion

    /**
     * Extract meta data about this gallery.
     * @returns {GalleryInfo} meta data object
     */
    function extractMetadata() {
        const lis = document.querySelectorAll("main > ul > li");
        const artist = lis[1].childNodes[0].innerText;
        const gallery = lis[2].childNodes[0].innerText;

        const date = new Date(document.querySelector("div.publication-date").innerText);
        const uploadDate = (date.getYear() + 1900) + (date.getMonth() + 1).toString() + date.getDate();

        const tags = [];
        document.querySelectorAll(".tags-list > li > a").forEach(tag => {
            tags.push(tag.innerText)
        });

        return new GalleryInfo(artist, uploadDate, gallery, tags.toString().replace("/", "-"));
    }

    /**
     * Execute a given action on all media on this page.
     * @param {Function} action Callback for each media on this page, arguments are (file URL {String}, override file name {String}, media element {Element})
     * @param {boolean} reverseOrder true to reverse the order in which links are passed to action.
     */
    function allMedia(action, reverseOrder) {
        const fileName = fillFileNameTemplate();
        let media = [...document.querySelectorAll(".gallery-photo img")].concat([...document.querySelectorAll("video > source")]);
        if (reverseOrder === true) media = media.reverse();
        for (let i = 0; i < media.length; i++) {
            const extension = media[i].src.substr(media[i].src.lastIndexOf("."));
            const name = fileName + (media.length > 1 ? ` (${i + 1})` : "") + extension;
            action(media[i].src, name, media[i])
        }
    }

    /**
     * Download a given source URL to a given name
     * @param {String} source the source of the file
     * @param {String} name the name to save the file as
     * @param {Element} element the related media element, used to display download state via styles
     */
    function download(source, name, element) {
        downloader.download(source, name,
            () => { element.style.border = `2px dotted ${FG_COLOR}` },
            (errorMessage) => {
                console.log(errorMessage);
                element.style.border = `2px dotted red`;
            },
            document.location.href
        );
    }

    /**
     * Open a given URL in a new tab
     * @param {String} url URL to open
     * @param {@String} overrideName override file name to use
     */
    function openInNewTab(url, overrideName) {
        GM_openInTab(url + "#" + overrideName);
    }

    /**
     * Create a button.
     * @param {String} text Text to be displayed on the button
     * @param {Function} onClick on click event handler for the button
     * @param {Boolean} useRoundedCorners set true to use rounded corner style; false or undefined to keep default corner style
     */
    function createButton(text, onClick, useRoundedCorners) {
        const button = document.createElement("button");
        button.innerText = text;
        button.addEventListener("click", onClick);
        if (useRoundedCorners) {
            button.classList.add("btn-rounded-pink");
        } else {
            button.classList.add("pink-btn");
        }
        button.style.fontWeight = "800";
        button.style.cursor = "pointer";
        return button;
    }

    /**
     * Fills the current file name template with metadata
     * @returns {String} the filled template
     */
    function fillFileNameTemplate() {
        return fileNameTemplate.replace("$artist", metaData.artist)
            .replace("$uploadDate", metaData.uploadDate)
            .replace("$galleryName", metaData.galleryName)
            .replace("$tags", metaData.tags);
    }

    /**
     * Displays a modal dialog to change the settings.
     * @todo Could probably use some cleanup/refactoring on this, but it works for now.
     */
    function openSettingsDialog() {
        const modalWrapper = createModalWrapper();
        const modal = createModal();
        const templateInput = createModalInput();
        const copyOnDownloadWrapper = createModalCheckbox("copy_url_on_download",
            "Copy URL to clipboard when downloading gallery.", copyUrlOnDownload);

        const likeOnDownloadWrapper = createModalCheckbox("like_on_download",
            "Add gallery to favorites when downloading. (Login required)", favOnDownload);

        const favOnDownloadWrapper = createModalCheckbox("fav_on_download",
            "Like gallery when downloading. (Login required)", likeOnDownload);
        // General settings
        modal.appendChild(createModalHeader("General settings"));
        modal.appendChild(copyOnDownloadWrapper);
        modal.appendChild(likeOnDownloadWrapper);
        modal.appendChild(favOnDownloadWrapper);

        // File name template
        modal.appendChild(createModalHeader("Adjust file name template"));
        modal.appendChild(createModalHint("Use placeholders and/or static text to define the template for download file names."));
        modal.appendChild(document.createElement("br"));
        modal.appendChild(templateInput);
        modal.appendChild(document.createElement("br"));
        modal.appendChild(createModalHint("Available placeholders are:"));
        modal.appendChild(createModalHint(`
        $artist ........ Name of the artist/uploader
        $galleryName ... Name of the gallery/upload
        $uploadDate .... Date of upload in the format of yyyyMMdd
        $tags .......... A comma separated list of tags
        `, "code", "monospace"));
        modal.appendChild(createModalHint(`
            You may also use / to define a folder structure.
            Note that this may or may not be supported with your browser or userscript manager.
            `));
        modal.appendChild(document.createElement("br"));
        modal.appendChild(createButton("Save", evt => {

            saveSettingsValue(FILE_NAME_TEMPLATE_KEY, fileNameTemplate, templateInput.value, (newValue) => fileNameTemplate = newValue);
            saveSettingsValue(COPY_URL_ON_DOWNLOAD_KEY, copyUrlOnDownload, copyOnDownloadWrapper.firstChild.checked, (newValue) => copyUrlOnDownload = newValue);
            saveSettingsValue(LIKE_ON_DOWNLOAD_KEY, likeOnDownload, likeOnDownloadWrapper.firstChild.checked, (newValue) => likeOnDownload = newValue);
            saveSettingsValue(FAV_ON_DOWNLOAD_KEY, favOnDownload, favOnDownloadWrapper.firstChild.checked, (newValue) => favOnDownload = newValue);

            document.body.removeChild(modalWrapper);
        }));
        modal.appendChild(createButton("Cancel", evt => {
            document.body.removeChild(modalWrapper);
        }));

        // Finishing
        modalWrapper.appendChild(modal);
        document.body.appendChild(modalWrapper);

        function saveSettingsValue(key, oldValue, newValue, updateValueFunction) {
            if (newValue !== oldValue) {
                updateValueFunction(newValue);
                GM_setValue(key, newValue);
            }
        }

        function createModalWrapper() {
            const wrapper = document.createElement("div");
            wrapper.style.position = "fixed";
            wrapper.style.zIndex = "1";
            wrapper.style.left = 0;
            wrapper.style.top = 0;
            wrapper.style.width = "100vw";
            wrapper.style.height = "100vh";
            wrapper.style.overflow = "hidden";
            wrapper.style.backgroundColor = "rgba(0, 0, 0, 0.4)";
            return wrapper;
        }

        function createModal() {
            const modal = document.createElement("div");
            modal.style.backgroundColor = BG_COLOR;
            modal.style.margin = "15% auto";
            modal.style.padding = "20px";
            modal.style.border = `2px solid ${FG_COLOR}`;
            modal.style.width = "80vw";
            return modal;
        }

        function createModalHeader(text) {
            const header = document.createElement("h1");
            header.innerText = text;
            header.style.fontSize = "22px";
            header.style.color = FG_COLOR;
            header.style.margin = "20px";
            return header;
        }

        /**
         * Create a hint text for the modal dialog.
         * @param {String} text the text to be displayed
         * @param {String} tagType the type of tag to use for the hint, by default: span
         * @param {String} fontFamily font family style to use, leave undefined for default behaviour
         */
        function createModalHint(text, tagType, fontFamily) {
            const hint = document.createElement(tagType || "span");
            hint.innerText = text;
            hint.style.lineHeight = "18px";
            if (fontFamily !== undefined) {
                hint.style.fontFamily = fontFamily;
            }
            return hint;
        }

        function createModalInput() {
            const input = document.createElement("input");
            input.value = fileNameTemplate;
            input.style.width = "50%";
            input.style.margin = "20px 0";
            return input;
        }

        /**
         * Create a checkbox and label.
         * @param {String} id DOM id for the checkbox
         * @param {String} hint hint to be displayed with the checked
         * @param {Boolean} value  whether the checkbox is checked or not
         * @param {Function} onValueChange Callback when the value of the checkbox changes;
         * has a single paramter which is either true or false.
         * @returns {Element} a div wrapping both the checkbox and label
         */
        function createModalCheckbox(id, hint, value) {
            const wrapper = document.createElement("div");
            const checkbox = document.createElement("input");
            checkbox.type = "checkbox";
            checkbox.id = id;
            checkbox.name = id;
            checkbox.checked = value;
            checkbox.style.appearance = "checkbox";
            const label = document.createElement("label");
            label.for = id;
            label.innerText = hint;
            label.style.verticalAlign = "super";
            wrapper.appendChild(checkbox);
            wrapper.appendChild(label);
            return wrapper;
        }
    }
})();