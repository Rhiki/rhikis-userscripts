# Rhiki's Userscripts + Custom CSS
A collection of userscripts, custom CSS files and small libs to simplify userscript coding. 

_All scripts can be found in [/scripts](https://bitbucket.org/Rhiki/rhikis-userscripts/src/master/scripts/)_

_All custom CSS files can be found in [/css](https://bitbucket.org/Rhiki/rhikis-userscripts/src/master/css/)_

_All libs can be found in [/tools](https://bitbucket.org/Rhiki/rhikis-userscripts/src/master/tools/)_

---
## License
All scripts and other files you find in thise repo are licensed under the *DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE*, so.. do what the fuck you want to do. See also [LICENSE](https://bitbucket.org/Rhiki/rhikis-userscripts/src/master/LICENSE).

---
## Reporting bugs/issues
You can create a new issue for bug reports or questsions here: [Create new issue](https://bitbucket.org/Rhiki/rhikis-userscripts/issues/new).

---
## Scripts
### **nekonavi jp (ris)** / [click to install](https://bitbucket.org/Rhiki/rhikis-userscripts/raw/master/scripts/nekonavi_jp.user.js) 
Affects/runs on:

- `https://nekonavi.jp/catblog/archives/*`

Adds a download button (top and bottom share bar) to `/catblog` blog posts on nekonavi.jp, that will save all images on the current page. Files will be saved as in your Downloads folder as: `./nekonavi.jp/<BLOG_AUTHOR> - <POST_TITLE> page <#> (<POST_DATE>).<EXT>`

### **dropbox com (ris)** / [click to install](https://bitbucket.org/Rhiki/rhikis-userscripts/raw/master/scripts/dropbox_com.user.js) 
Affects/runs on:

- `https://www.dropbox.com/*`

Features:

- Hide some rather obnoxious footers.
- Add shortcuts to download files/galleries (`S` or `Numpad-5`).
- Add download functionality for galleries/files, that prohibit downloading. Note that this can only be accessed with the download shortcut key.
    - Support for this is still experimental. If you encounter any problems, feel free to let me know, see section *Reporting bugs/issues* at the top of this document.
    - For single/previewd images: Will download the image with the original file name.
    - For single/previewed videos: Will show you a list of available qualities (m3u8) and offer [ffmpeg](https://ffmpeg.org/) scripts to download them.
    - For image galleries:
        - Iterates through all list items and downloads each one of them.
        - Progress is reported in the headline, in addition each file gets a border with a colorcoded status:
            - Purple: download ongoing
            - Green: successfully downloaded
            - Red: download failed
        - If any downloads failed, you can simply start the download again and the script will only try to download files that failed previously. If you wish to do a complete redownload you'll have to reload the page.
    - Other file types in galleries aren't supported yet.

### **4chan.org + 4chan X (ris)** / [click to install](https://bitbucket.org/Rhiki/rhikis-userscripts/raw/master/scripts/4chan_org.user.js) 
Affects/runs on:

- `https://boards.4chan.org/*/thread/*`
- `https://boards.4channel.org/*/thread/*`

Works on top of 4chan X, the whole userscript (or maybe only some functionality) will break if you don't have it installed.

- Add more keybinds to the 4chan X gallery view:
    - **S**, **Arrow Down** and **Numpad 5**: Download the current file, with a special filename template. _TODO: Not customizable yet._
    - **A** and **D**: Can be used to navigate the gallery, same as **Arrow Left** and **Arrow Right**.
    - **C**: Copy the link to the current file to your clipboard.
- Add a list of all links in the thread to the top right corner, you may have to refresh it with the included button to see all links if the thread updates via 4chan X.
    - Version 1.1.0: Collapsible/Expandable link list.
    - Version 1.2.0: Add button to scroll the post containing the link into view.
    - Version 1.2.1: Use a different color for already visited links in the thread list.
    - _TODO: Auto-update when new posts are being added to the thread.
- Further TODOs:
    - _TODO: Customizable user settings._
    - _TODO: Add a button to download all files in the current thread._
    - _Maybe TODO: Overwrite 4chan X behaviour when clicking on the file name link in the gallery (change to same saving behaviour as S key)._


### **slushe.com (ris)** / [click to install](https://bitbucket.org/Rhiki/rhikis-userscripts/raw/master/scripts/slushe_com.user.js) 
Affects/runs on:

- `https://slushe.com/galleries/*`
- `https://slushe.com/video/*`

Add a button + keybind (S) to download the current gallery with customizable file name formatting.
- Version 1.2.0:
  - Fix HTTP 403 errors by setting the referer header to the current URL.
  - Add option to like/fav a gallery when downloading it (login required).

### **bgeekgirls.com (ris)** / [click to install](https://bitbucket.org/Rhiki/rhikis-userscripts/raw/master/scripts/bgeekgirls_com.user.js) 
Affects/runs on:

- `https://bgeekgirls.com/galleries/*`

Add a button to download gallery.

### **facebook.com (ris)** / [click to install](https://bitbucket.org/Rhiki/rhikis-userscripts/raw/master/scripts/facebook_com.user.js) 
Affects/runs on:

- `https://www.facebook.com` (feed only)

Remove sponsored posts from your feed.

### **kemono.party (ris)** / [click to install](https://bitbucket.org/Rhiki/rhikis-userscripts/raw/master/scripts/kemono_party.user.js) 
Affects/runs on:

- User/artist page:`https://kemono.party/*/user/*`
    - Highlights all previously seen (does not equal visited) posts with an orange border.
- Post page: `https://kemono.party/*/user/*/post/*`
    - Button to open all links in the post.
    - Button to save all attached files in the post.
    - Add a list of all links in the post:
        - Internal: List of attached files + button to download each file separately.
        - External: List of all links to external hosts, for example mega.nz, Google Drive, YouTube, etc.
    - Download buttons will save the files to `~/Downloads/kemono.party/<ARTIST> (<ID>)/<FILE>` by default.
        - TODO: Configurable.

### **streamable.com (ris)** / [click to install](https://bitbucket.org/Rhiki/rhikis-userscripts/raw/master/scripts/streamable_com.user.js) 
Affects/runs on:

- `https://streamable.com/*`

Add a button to download the current video with the title as filename.

### **yiff party (ris)** / [click to install](https://bitbucket.org/Rhiki/rhikis-userscripts/raw/master/scripts/yiff.party/ris-yiff_party.user.js) 
Affects/runs on:

- `https://yiff.party/*`
- `https://data.yiff.party/*`

RIP

Improved lurking on yiff.party. For a more detailed description and information checkout this [README.md](https://bitbucket.org/Rhiki/rhikis-userscripts/src/master/scripts/yiff.party/README.md).

---

## CSS
### [kaycee.css](https://bitbucket.org/Rhiki/rhikis-userscripts/src/master/css/kaycee.css)

Affects/runs on:

- If you consider using this you should probably know.

Mainly dark-mode theme, but can be easily adjusted to any color scheme of your liking.

---

## Tools
### [download.js](https://bitbucket.org/Rhiki/rhikis-userscripts/src/master/tools/download.js)
Small lib to simplify downloads via GM_download, as well as supporting template file names.