/**
 * Util class to simplify downloading via userscript and supporting template filenames.
 */
class Downloader {

    constructor(template) {
        this.template = template;
        this.queue = [];
        this.enqueue = false;
    }

    enableQueue() {
        this.enqueue = true;
    }

    disableQueue() {
        this.enqueue = false;
    }

    /**
     * Downloads a given `src` to `fileName`.
     * @param {string} src the file to download
     * @param {string} fileName the name as which the file should be saved
     * @param {function():void} onSuccess callback on successful download; optional
     * @param {function(string):void} onFailure callback on failed download, contains an error message; optional
     * @param {string} referer string to be used as referer; optional, if `undefined` the header will not be set
     * detailing the error; optional
     */
    download(src, fileName, onSuccess, onFailure, referer) {
        this._download(src, fileName, onSuccess, onFailure, referer);
    }

    /**
     * Downloads a given `src` and downloads it to a file name that will be created from `this.template`
     * and parameters from `fileNameParamters`.
     * @param {string} src the file to download
     * @param {TemplateParameters} fileNameParameters the name as which the file should be saved
     * @param {function():void} onSuccess callback on successful download; optional
     * @param {function(string):void} onFailure callback on failed download, contains an error message; optional
     * @param {string} referer string to be used as referer; optional, if `undefined` the header will not be set
     * detailing the error; optional
     */
    downloadWithTemplate(src, fileNameParameters, onSuccess, onFailure, referer) {
        this._download(src, this._fillTemplate(fileNameParameters), onSuccess, onFailure, referer);
    }

    /**
     * Implements the actual download logic.
     * @param {string} src the file to download
     * @param {string} fileName the name as which the file should be saved
     * @param {function():void} onSuccess callback on successful download; optional
     * @param {function(string):void} onFailure callback on failed download, contains an error message; optional
     * @param {string} referrer string to be used as referer; optional, if `undefined` the header will not be set
     * detailing the error; optional
     */
    _download(src, fileName, onSuccess, onFailure, referrer) {
        const downloadDetails = {
            url: src,
            name: fileName
        }
        if (onSuccess !== undefined) {
            downloadDetails.onload = () => onSuccess();
        }
        if (onFailure !== undefined) {
            downloadDetails.onerror = (error) => onFailure(`Download failed for: ${src} => ${fileName}. Reason: ${error.error}`);
            downloadDetails.ontimeout = () => onFailure(`Download timed out for: ${src}.`)
        }
        if (referrer !== undefined) {
            downloadDetails.headers = { referer: referrer }
        }
        if (this.enqueue) {
            this.queue.push(downloadDetails);
        } else {
            GM_download(downloadDetails);
        }
    }

    downloadQueue() {
        this._downloadQueue();
    }

    _downloadQueue() {
        const details = this.queue.shift();
        if (details !== undefined) {
            interceptCallback(details, "onload", () => this._downloadQueue());
            interceptCallback(details, "onerror", () => this._downloadQueue());
            interceptCallback(details, "ontimeout", () => this._downloadQueue());
            GM_download(details);
        }

        function interceptCallback(object, callbackName, continuation) {
            const temp = object[callbackName];
            // onerror callback uses arg1 so we have to pass it here, it shouldnt interfer with the other callbacks.
            object[callbackName] = (arg1) => {
                temp(arg1);
                continuation();
            }
        }
    }



    /**
     * Fills the `this.template` string with the parameters provided by the `parameters` argument.
     * @param {TemplateParameters} parameters 
     */
    _fillTemplate(parameters) {
        let fileName = this.template;
        parameters.forEach((placeHolder, value) => {
            fileName = fileName.replaceAll(placeHolder, value);
        });
        return fileName;
    }
}

class TemplateParameters {

    constructor() {
        this.values = [];
    }

    addParameter(placeHolder, value) {
        this.values.push({ placeHolder: placeHolder, value: value });
        return this;
    }

    /**
     * Iterates over all template parameters of this object.
     * @param {function(any, any):void} callback Called for each parameter with the paramter order: placeHolder, value
     */
    forEach(callback) {
        for (let i = 0; i < this.values.length; i++) {
            callback(this.values[i].placeHolder, this.values[i].value);
        }
    }
}